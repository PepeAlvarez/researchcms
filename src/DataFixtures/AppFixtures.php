<?php
/**
 * Created by PhpStorm.
 * User: pepe
 * Date: 12/10/18
 * Time: 18:48
 */

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Member;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $member = new Member();

        $member->setFirstName('admin');
        $member->setLastName('admin');
        $member->setUsername('admin');
        $member->setEmail('admin@mail.com');
        $member->setIsActive(true);
        $member->setRoles(array('ROLE_ADMIN', 'ROLE_USER'));

        $password = $this->encoder->encodePassword($member, 'admin');
        $member->setPassword($password);

        $manager->persist($member);
        $manager->flush();
    }

}