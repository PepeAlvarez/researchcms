<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */


/**
 * Authorship
 *
 * @ORM\Table(name="authorships", uniqueConstraints={@ORM\UniqueConstraint(name="ix_member_publication", columns={"member_id","publication_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\AuthorshipRepository")
 */
class Authorship
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Member", inversedBy="authorships",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $member;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Publication", inversedBy="authorships",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $publication;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('author','assignee','editor','chair-or-translator','co-investigator','co-inventor','graduate-student','other-inventor','principal-investigator','postdoctoral-researcher','support-staff')", length=30)
     */
    private $typeOfAuthorship;

    public function getId()
    {
        return $this->id;
    }

    public function getMember(): ?Member
    {
        return $this->member;
    }

    public function setMember(?Member $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getPublication(): ?Publication
    {
        return $this->publication;
    }

    public function setPublication(?Publication $publication): self
    {
        $this->publication = $publication;

        return $this;
    }

    public function getTypeOfAuthorship(): ?string
    {
        return $this->typeOfAuthorship;
    }

    public function setTypeOfAuthorship(string $typeOfAuthorship): self
    {
        $this->typeOfAuthorship = $typeOfAuthorship;

        return $this;
    }
}
