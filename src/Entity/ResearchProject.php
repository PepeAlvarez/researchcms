<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * ResearchProject
 *
 * @ORM\Table(name="researchprojects")
 * @ORM\Entity(repositoryClass="App\Repository\ResearchProjectRepository")
 * @UniqueEntity(fields="title", message="Title already taken")
 */
class ResearchProject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255), unique=true
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=100, nullable=true, unique=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $financialEntity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $amount;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $participatingEntities;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Member", inversedBy="researchProjectsResponsible")
     */
    private $participantResponsible;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Member", inversedBy="researchProjects")
     */
    private $participants;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $unescoCodes;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $technologicalAreas;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cnaeCodes;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $picture1;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $picture2;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $picture3;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Collaborator", inversedBy="researchProjects")
     */
    private $collaborators;


    public function __construct()
    {
        $this->participants = new ArrayCollection();
        $this->collaborators = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getFinancialEntity(): ?string
    {
        return $this->financialEntity;
    }

    public function setFinancialEntity(?string $financialEntity): self
    {
        $this->financialEntity = $financialEntity;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getParticipatingEntities(): ?string
    {
        return $this->participatingEntities;
    }

    public function setParticipatingEntities(?string $participatingEntities): self
    {
        $this->participatingEntities = $participatingEntities;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getParticipantResponsible(): ?Member
    {
        return $this->participantResponsible;
    }

    public function setParticipantResponsible(?Member $participantResponsible): self
    {
        $this->participantResponsible = $participantResponsible;

        return $this;
    }

    /**
     * @return Collection|Member[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(Member $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants[] = $participant;
            $participant->addResearchProject($this);
        }

        return $this;
    }

    public function removeParticipant(Member $participant): self
    {
        if ($this->participants->contains($participant)) {
            $this->participants->removeElement($participant);
            $participant->removeResearchProject($this);
        }

        return $this;
    }

    public function getUnescoCodes(): ?string
    {
        return $this->unescoCodes;
    }

    public function setUnescoCodes(?string $unescoCodes): self
    {
        $this->unescoCodes = $unescoCodes;

        return $this;
    }

    public function getTechnologicalAreas(): ?string
    {
        return $this->technologicalAreas;
    }

    public function setTechnologicalAreas(?string $technologicalAreas): self
    {
        $this->technologicalAreas = $technologicalAreas;

        return $this;
    }

    public function getCnaeCodes(): ?string
    {
        return $this->cnaeCodes;
    }

    public function setCnaeCodes(?string $cnaeCodes): self
    {
        $this->cnaeCodes = $cnaeCodes;

        return $this;
    }

    public function getPicture1()
    {
        if ($this->picture1 != null) {
            if (is_string($this->picture1)) {
                $image = $this->picture1;
            } else {
                $image = base64_encode(stream_get_contents($this->picture1));
            }
        } else {
            $image = null;
        }
        return $image;
    }

    public function setPicture1($picture1): self
    {
        $this->picture1 = $picture1;

        return $this;
    }

    public function getPicture2()
    {
        if ($this->picture2 != null) {
            if (is_string($this->picture2)) {
                $image = $this->picture2;
            } else {
                $image = base64_encode(stream_get_contents($this->picture2));
            }
        } else {
            $image = null;
        }
        return $image;
    }

    public function setPicture2($picture2): self
    {
        $this->picture2 = $picture2;

        return $this;
    }

    public function getPicture3()
    {
        if ($this->picture3 != null) {
            if (is_string($this->picture3)) {
                $image = $this->picture3;
            } else {
                $image = base64_encode(stream_get_contents($this->picture3));
            }
        } else {
            $image = null;
        }
        return $image;
    }

    public function setPicture3($picture3): self
    {
        $this->picture3 = $picture3;

        return $this;
    }

    /**
     * @return Collection|Collaborator[]
     */
    public function getCollaborators(): Collection
    {
        return $this->collaborators;
    }

    public function addCollaborator(Collaborator $collaborator): self
    {
        if (!$this->collaborators->contains($collaborator)) {
            $this->collaborators[] = $collaborator;
            $collaborator->addResearchProject($this);
        }

        return $this;
    }

    public function removeCollaborator(Collaborator $collaborator): self
    {
        if ($this->collaborators->contains($collaborator)) {
            $this->collaborators->removeElement($collaborator);
            $collaborator->removeResearchProject($this);
        }

        return $this;
    }

}