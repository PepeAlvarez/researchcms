<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @ORM\Entity(repositoryClass="App\Repository\HomeRepository")
 */
class Home
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mainName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $extendedName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $definition;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $logo;


    public function getId()
    {
        return $this->id;
    }

    public function getMainName(): ?string
    {
        return $this->mainName;
    }

    public function setMainName(string $mainName): self
    {
        $this->mainName = $mainName;

        return $this;
    }

    public function getExtendedName(): ?string
    {
        return $this->extendedName;
    }

    public function setExtendedName(?string $extendedName): self
    {
        $this->extendedName = $extendedName;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(?string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getLogo()
    {
        if ($this->logo != null) {
            if (is_string($this->logo)) {
                $image = $this->logo;
            } else {
                $image = base64_encode(stream_get_contents($this->logo));
            }
        } else {
            $image = null;
        }
        return $image;
    }

    public function setLogo($logo): self
    {
        //if (is_string($pictureProfile)) {
        //    $this->pictureProfile = base64_decode($pictureProfile);
        //} else {
        $this->logo = $logo;
        //}
        return $this;
    }
}
