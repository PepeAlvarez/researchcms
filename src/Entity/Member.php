<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */


/**
 * Member
 *
 * @ORM\Table(name="members", uniqueConstraints={@ORM\UniqueConstraint(name="ix_first_name_last_name", columns={"first_name","last_name"})})
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 * @ORM\HasLifecycleCallbacks()
 */
class Member implements UserInterface//, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    private $pictureProfile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $qualifications;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $occupation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $subjects;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $membership;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $googleScholarId;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $scopusId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mendeleyId;

    /**
     * @ORM\Column(type="string", length=19, nullable=true)
     */
    private $orcidId;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $researcherId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hIndex;

    /**
     * @ORM\Column(type="string", length=235, nullable=true)
     */
    private $personalWebSite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $positions;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $other;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ResearchProject", mappedBy="participantResponsible")
     */
    private $researchProjectsResponsible;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ResearchProject", mappedBy="participants")
     */
    private $researchProjects;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Authorship", mappedBy="member", orphanRemoval=true, cascade={"persist"})
     */
    private $authorships;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    //the json type, that is supposed to be the next one, causes error in MariaDB.
    //Developers recommend change it for TEXT type
    //https://mariadb.com/kb/en/library/json-data-type/
    /**
     * @ORM\Column(type="text")
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="author")
     */
    private $posts;


    public function __construct()
    {
        $this->researchProjectsResponsible = new ArrayCollection();
        $this->researchProjects = new ArrayCollection();
        $this->authorships = new ArrayCollection();
        $this->isActive = true;
        $this->posts = new ArrayCollection();
        //$this->roles = array('ROLE_USER');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPictureProfile()
    {
        if ($this->pictureProfile != null) {
            if (is_string($this->pictureProfile)) {
                $image = $this->pictureProfile;
            } else {
                $image = base64_encode(stream_get_contents($this->pictureProfile));
            }
        } else {
            $image = null;
        }
        return $image;
    }

    public function setPictureProfile($pictureProfile): self
    {
        //if (is_string($pictureProfile)) {
        //    $this->pictureProfile = base64_decode($pictureProfile);
        //} else {
            $this->pictureProfile = $pictureProfile;
        //}
        return $this;
    }

    public function getQualifications(): ?string
    {
        return $this->qualifications;
    }

    public function setQualifications(?string $qualifications): self
    {
        $this->qualifications = $qualifications;

        return $this;
    }

    public function getOccupation(): ?string
    {
        return $this->occupation;
    }

    public function setOccupation(?string $occupation): self
    {
        $this->occupation = $occupation;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getSubjects(): ?string
    {
        return $this->subjects;
    }

    public function setSubjects(?string $subjects): self
    {
        $this->subjects = $subjects;

        return $this;
    }

    public function getMembership(): ?string
    {
        return $this->membership;
    }

    public function setMembership(?string $membership): self
    {
        $this->membership = $membership;

        return $this;
    }

    public function getGoogleScholarId(): ?string
    {
        return $this->googleScholarId;
    }

    public function setGoogleScholarId(?string $googleScholarId): self
    {
        $this->googleScholarId = $googleScholarId;

        return $this;
    }

    public function getScopusId(): ?string
    {
        return $this->scopusId;
    }

    public function setScopusId(?string $scopusId): self
    {
        $this->scopusId = $scopusId;

        return $this;
    }

    public function getMendeleyId(): ?string
    {
        return $this->mendeleyId;
    }

    public function setMendeleyId(?string $mendeleyId): self
    {
        $this->mendeleyId = $mendeleyId;

        return $this;
    }

    public function getOrcidId(): ?string
    {
        return $this->orcidId;
    }

    public function setOrcidId(?string $orcidId): self
    {
        $this->orcidId = $orcidId;

        return $this;
    }

    public function getResearcherId(): ?string
    {
        return $this->researcherId;
    }

    public function setResearcherId(?string $researcherId): self
    {
        $this->researcherId = $researcherId;

        return $this;
    }

    public function getHIndex(): ?int
    {
        return $this->hIndex;
    }

    public function setHIndex(?int $hIndex): self
    {
        $this->hIndex = $hIndex;

        return $this;
    }

    public function getPersonalWebSite(): ?string
    {
        return $this->personalWebSite;
    }

    public function setPersonalWebSite(?string $personalWebSite): self
    {
        $this->personalWebSite = $personalWebSite;

        return $this;
    }

    public function getPositions(): ?string
    {
        return $this->positions;
    }

    public function setPositions(?string $positions): self
    {
        $this->positions = $positions;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function SetCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function SetUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }


    public function getFullName(): ?string
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }

    /**
     * @return Collection|ResearchProject[]
     */
    public function getResearchProjectsResponsible(): Collection
    {
        return $this->researchProjectsResponsible;
    }

    public function addResearchProjectsResponsible(ResearchProject $researchProjectsResponsible): self
    {
        if (!$this->researchProjectsResponsible->contains($researchProjectsResponsible)) {
            $this->researchProjectsResponsible[] = $researchProjectsResponsible;
            $researchProjectsResponsible->setParticipantResponsible($this);
        }

        return $this;
    }

    public function removeResearchProjectsResponsible(ResearchProject $researchProjectsResponsible): self
    {
        if ($this->researchProjectsResponsible->contains($researchProjectsResponsible)) {
            $this->researchProjectsResponsible->removeElement($researchProjectsResponsible);
            // set the owning side to null (unless already changed)
            if ($researchProjectsResponsible->getParticipantResponsible() === $this) {
                $researchProjectsResponsible->setParticipantResponsible(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ResearchProject[]
     */
    public function getResearchProjects(): Collection
    {
        return $this->researchProjects;
    }

    public function addResearchProject(ResearchProject $researchProject): self
    {
        if (!$this->researchProjects->contains($researchProject)) {
            $this->researchProjects[] = $researchProject;
            $researchProject->addParticipant($this);
        }

        return $this;
    }

    public function removeResearchProject(ResearchProject $researchProject): self
    {
        if ($this->researchProjects->contains($researchProject)) {
            $this->researchProjects->removeElement($researchProject);
            $researchProject->removeParticipant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Authorship[]
     */
    public function getAuthorships(): Collection
    {
        return $this->authorships;
    }        //$this->authors = new ArrayCollection();

    public function addAuthorship(Authorship $authorship): self
    {
        if (!$this->authorships->contains($authorship)) {
            $this->authorships[] = $authorship;
            $authorship->setMember($this);
        }

        return $this;
    }

    public function removeAuthorship(Authorship $authorship): self
    {
        if ($this->authorships->contains($authorship)) {
            $this->authorships->removeElement($authorship);
            // set the owning side to null (unless already changed)
            if ($authorship->getMember() === $this) {
                $authorship->setMember(null);
            }
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

//Documentation says an array called "roles" must exist which contains each member's roles. This array must be implemented in the database as an "array" or "json" datatype, but "MariaDB" manager doesn't allow them. The developed solution declares the "roles" property of "Member" as "text" type, (represented in the database as "longtext" type), and modify the get and set methods of that property as the documentation dictates, the way that "get" method transforms the stored roles as separated by comma text in an array using the "explode" function, and viceversa with the "set" method and the function "implode".

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = explode(",", $this->roles);
        // guarantee every user at least has ROLE_USER
        array_push($roles,'ROLE_USER');

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = implode(",",$roles);

        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {

    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setAuthor($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getAuthor() === $this) {
                $post->setAuthor(null);
            }
        }

        return $this;
    }

    //This is part is no more needed
//    /** @see \Serializable::serialize() */
//    public function serialize()
//    {
//        return serialize(array(
//            $this->id,
//            $this->username,
//            $this->password,
//            // see section on salt below
//            // $this->salt,
//        ));
//    }

//    /** @see \Serializable::unserialize() */
//    public function unserialize($serialized)
//    {
//        list (
//            $this->id,
//            $this->username,
//            $this->password,
//            // see section on salt below
//            // $this->salt
//            ) = unserialize($serialized, array('allowed_classes' => false));
//    }

    /* Only for AdvancedUserInterface, that is disapproved in Symfony 4
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }
    */

    /*public function __toString()
    {
        // to show the name of the Category in the select
        return $this->getPictureProfile();
        // to show the id of the Category in the select
        // return $this->id;
    }*/
}
