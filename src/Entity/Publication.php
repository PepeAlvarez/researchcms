<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * Publication
 *
 * @ORM\Table(name="publications", uniqueConstraints={@ORM\UniqueConstraint(name="ix_title_type_year", columns={"title","type","year"})})
 * @ORM\Entity(repositoryClass="App\Repository\PublicationRepository")
 */
class Publication
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('artistic-performance','book-chapter','book-review','book','conference-abstract','conference-paper','conference-poster','data-set','dictionary-entry','disclosure','dissertation','edited-book','encyclopedia-entry','invention','journal-article','journal-issue','lecture-speech','license','magazine-article','manual','newsletter-article','newspaper-article','online-resource','other','patent','registered-copyright','report','research-technique','research-tool','spin-off-company','standards-and-policy','supervised-student-publication','technical-standard','test','translation','trademark','website','working-paper')", length=35)
     */
    private $type;

    /**
     * @ORM\Column(type="string",length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=240, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $doi;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $issn;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $journal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $volume;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $pages;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $abstract;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $keywords;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $pubstate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $bibTex;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Authorship", mappedBy="publication", orphanRemoval=true, cascade={"persist"})
     */
    private $authorships;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $eid;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authors;

    public function __construct()
    {
        $this->authorships = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getDoi(): ?string
    {
        return $this->doi;
    }

    public function setDoi(?string $doi): self
    {
        $this->doi = $doi;

        return $this;
    }

    public function getIssn(): ?string
    {
        return $this->issn;
    }

    public function setIssn(?string $issn): self
    {
        $this->issn = $issn;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getJournal(): ?string
    {
        return $this->journal;
    }

    public function setJournal(?string $journal): self
    {
        $this->journal = $journal;

        return $this;
    }

    public function getVolume(): ?int
    {
        return $this->volume;
    }

    public function setVolume(?int $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getPages(): ?string
    {
        return $this->pages;
    }

    public function setPages(?string $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    public function getAbstract(): ?string
    {
        return $this->abstract;
    }

    public function setAbstract(?string $abstract): self
    {
        $this->abstract = $abstract;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getPubstate(): ?string
    {
        return $this->pubstate;
    }

    public function setPubstate(?string $pubstate): self
    {
        $this->pubstate = $pubstate;

        return $this;
    }

    public function getBibTex(): ?string
    {
        return $this->bibTex;
    }

    public function setBibTex(?string $bibTex): self
    {
        $this->bibTex = $bibTex;

        return $this;
    }

    /**
     * @return Collection|Authorship[]
     */
    public function getAuthorships(): Collection
    {
        return $this->authorships;
    }

    public function addAuthorship(Authorship $authorship): self
    {
        if (!$this->authorships->contains($authorship)) {
            $this->authorships[] = $authorship;
            $authorship->setPublication($this);
        }

        return $this;
    }

    public function removeAuthorship(Authorship $authorship): self
    {
        if ($this->authorships->contains($authorship)) {
            $this->authorships->removeElement($authorship);
            // set the owning side to null (unless already changed)
            if ($authorship->getPublication() === $this) {
                $authorship->setPublication(null);
            }
        }

        return $this;
    }

    public function getEid(): ?string
    {
        return $this->eid;
    }

    public function setEid(?string $eid): self
    {
        $this->eid = $eid;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(?string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getAuthors(): ?string
    {
        return $this->authors;
    }

    public function setAuthors(?string $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

}
