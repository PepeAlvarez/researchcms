<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @ORM\Entity(repositoryClass="App\Repository\CollaboratorRepository")
 */

/**
 * Collaborator
 *
 * @ORM\Table(name="collaborators")
 * @ORM\Entity(repositoryClass="App\Repository\CollaboratorRepository")
 */

class Collaborator
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=235, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ResearchProject", mappedBy="collaborators")
     */
    private $researchProjects;

    public function __construct()
    {
        $this->researchProjects = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }


    public function getFullName(): ?string
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }

    /**
     * @return Collection|ResearchProject[]
     */
    public function getResearchProjects(): Collection
    {
        return $this->researchProjects;
    }

    public function addResearchProject(ResearchProject $researchProject): self
    {
        if (!$this->researchProjects->contains($researchProject)) {
            $this->researchProjects[] = $researchProject;
            $researchProject->addCollaborator($this);
        }

        return $this;
    }

    public function removeResearchProject(ResearchProject $researchProject): self
    {
        if ($this->researchProjects->contains($researchProject)) {
            $this->researchProjects->removeElement($researchProject);
            $researchProject->removeCollaborator($this);
        }

        return $this;
    }
}
