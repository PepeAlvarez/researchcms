<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Home;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use App\Repository\HomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Translation\TranslatorInterface;
//use Symfony\Component\EventDispatcher\EventSubscriberInterface;
//use Symfony\Component\HttpFoundation\Session\SessionInterface;
//use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
//use Symfony\Component\Security\Http\SecurityEvents;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/contact", defaults = {"_locale" = "en"})
 */
class ContactController extends Controller
{
    /*private $session;
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }*/
    /**
     * @Route("/pub", name="contact_pub", methods="GET")
     */
    public function pub(ContactRepository $contactRepository, HomeRepository $homeRepository, TranslatorInterface $translator, Request $request): Response
    {
        //$locale = $request->getLocale();

        //var_dump($locale);

        //$this->get('session')->set('_locale', 'es_ES');

        //$locale = $this->get('session')->get('_locale');

        //var_dump($locale);
       // $locale = $request->getPreferredLanguage();//catching browser language
       // $this->session->set('_locale', $locale);
        //$locale = $this->session->get('_locale');
       // var_dump($locale);


        //$request=$this->getRequest();
        //$locale = $request->getPreferredLanguage();//catching browser language
        //var_dump($locale);
        //$request->setLocale($locale);
        //$this->get('session')->set('_locale', $locale);


        $homedata = $homeRepository->findAll();
        $contactdata = $contactRepository->findAll();

        if(empty($homedata) OR is_null($homedata)) {
            $homedata[] = new Home();
            $homedata[0]
                ->setMainName($translator->trans("[Your research group name here]"))
                ->setExtendedName($translator->trans("[Your research group extended name here]"))
                ->setDefinition($translator->trans("[Your research group definition here]"));
        }

        if(empty($contactdata) OR is_null($contactdata)) {
            $contactdata[] = new Contact();
            $contactdata[0]
                ->setFirstLine($translator->trans("[Your contact first line here]"))
                ->setSecondLine($translator->trans("[Your contact second line here]"))
                ->setAddress($translator->trans("[Your contact address here]"))
                ->setPhone($translator->trans("[Your contact phone here]"));
        }
        return $this->render('contact/pub.html.twig', ['contacts' => $contactdata,'homes' => $homedata ]);
    }

    /**
     * @Route("/", name="contact_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(ContactRepository $contactRepository): Response
    {
        return $this->render('contact/index.html.twig', ['contacts' => $contactRepository->findAll()]);
    }

    /**
     * @Route("/new", name="contact_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            return $this->redirectToRoute('contact_index');
        }

        return $this->render('contact/new.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contact_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Contact $contact): Response
    {
        return $this->render('contact/show.html.twig', ['contact' => $contact]);
    }

    /**
     * @Route("/{id}/edit", name="contact_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Contact $contact): Response
    {
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contact_edit', ['id' => $contact->getId()]);
        }

        return $this->render('contact/edit.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contact_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Contact $contact): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contact->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contact);
            $em->flush();
        }

        return $this->redirectToRoute('contact_index');
    }
}
