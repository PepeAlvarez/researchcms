<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Home;
use App\Repository\HomeRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

class NavBarController extends Controller
{

    public function index(HomeRepository $homeRepository, TranslatorInterface $translator): Response
    {
        $allrgdata = $homeRepository->findAll();

        if(empty($allrgdata) OR is_null($allrgdata)) {
            $rgdata["name"] = $translator->trans("[Your research group name here]");
            $rgdata["logo"] = $translator->trans("[Your logo here]");
        }
        else {
            $rgdata["name"] = $allrgdata[0]->getMainName();
            $rgdata["logo"] = $allrgdata[0]->getLogo();
        }

        return $this->render('inc/navbar.html.twig', ['rgdata' => $rgdata]);
    }
}
