<?php

namespace App\Controller;

use App\Entity\Collaborator;
use App\Form\CollaboratorType;
use App\Repository\CollaboratorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/collaborator", defaults = {"_locale" = "en"})
 */
class CollaboratorController extends Controller
{
    /**
     * @Route("/pub", name="collaborator_pub", methods="GET")
     */
    public function pub(CollaboratorRepository $collaboratorRepository): Response
    {
        return $this->render('collaborator/pub.html.twig', ['collaborators' => $collaboratorRepository->findAll()]);
    }

    /**
     * @Route("/pub/{id}", name="collaborator_pub_show", methods="GET")
     */
    public function pubshow(Collaborator $collaborator, Request $request, CollaboratorRepository $collaboratorRepository): Response
    {
        $collaborator_id = $request->attributes->get('_route_params')['id'];
        $collaborator = $collaboratorRepository->findOneBy(['id' => $collaborator_id]);

        return $this->render('collaborator/pubshow.html.twig', ['collaborator' => $collaborator]);
    }

    /**
     * @Route("/", name="collaborator_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(CollaboratorRepository $collaboratorRepository): Response
    {
        return $this->render('collaborator/index.html.twig', ['collaborators' => $collaboratorRepository->findAll()]);
    }

    /**
     * @Route("/new", name="collaborator_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $collaborator = new Collaborator();
        $form = $this->createForm(CollaboratorType::class, $collaborator);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($collaborator);
            $em->flush();

            return $this->redirectToRoute('collaborator_index');
        }

        return $this->render('collaborator/new.html.twig', [
            'collaborators' => $collaborator,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="collaborator_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Collaborator $collaborator): Response
    {
        return $this->render('collaborator/show.html.twig', ['collaborators' => $collaborator]);
    }

    /**
     * @Route("/{id}/edit", name="collaborator_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Collaborator $collaborator): Response
    {
        $form = $this->createForm(CollaboratorType::class, $collaborator);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('collaborator_edit', ['id' => $collaborator->getId()]);
            return $this->redirectToRoute('collaborator_index');
        }

        return $this->render('collaborator/edit.html.twig', [
            'collaborators' => $collaborator,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="collaborator_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Collaborator $collaborator): Response
    {
        if ($this->isCsrfTokenValid('delete'.$collaborator->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($collaborator);
            $em->flush();
        }

        return $this->redirectToRoute('collaborator_index');
    }
}
