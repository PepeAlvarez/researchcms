<?php
    namespace App\Controller;

    use App\Entity\Home;
    use App\Entity\ResearchLine;
    use App\Repository\HomeRepository;
    use App\Repository\ResearchLineRepository;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Translation\TranslatorInterface;

    /*
     * This file is part of the ResearchCMS project
     */
    /**
     * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
     */

    class StartController extends Controller {


        /**
         * @Route("/{_locale}", defaults = {"_locale" = "en"})
         * @Method({"GET"})
         */
        public function index(HomeRepository $homeRepository, ResearchLineRepository $researchLineRepository, TranslatorInterface $translator): Response
        {
            $homedata = $homeRepository->findAll();

            $researchlinesdata = $researchLineRepository->findAll();

            if(empty($homedata) OR is_null($homedata)) {
                $rgdata["mainName"] = $translator->trans("[Your research group name here]");
                $rgdata["logo"] = $translator->trans("[Your logo here]");
                $rgdata["extendedName"] = $translator->trans("[Your research group extended name here]");
                $rgdata["definition"] = $translator->trans("[Your research group definition here]");
            }
            else {
                $rgdata["mainName"] = $homedata[0]->getMainName();
                $rgdata["logo"] = $homedata[0]->getLogo();
                $rgdata["extendedName"] = $homedata[0]->getExtendedName();
                $rgdata["definition"] = $homedata[0]->getDefinition();
            }

            if(empty($researchlinesdata) OR is_null($researchlinesdata)) {
                $researchlinesdata[] = new ResearchLine();
                $researchlinesdata[0]
                    ->setTitle($translator->trans("[Research line 1 here]"))
                    ->setExplanation($translator->trans("[Research line 1 explantion here]"));
                $researchlinesdata[] = new ResearchLine();
                $researchlinesdata[1]
                    ->setTitle($translator->trans("[Research line 2 here]"))
                    ->setExplanation($translator->trans("[Research line 2 explantion here]"));
            }

            //return $this->render('start/index.html.twig', ['start' => $homedata[0], 'lines' => $researchlinesdata]);
            return $this->render('start/index.html.twig', ['start' => $rgdata, 'lines' => $researchlinesdata]);
        }
    }