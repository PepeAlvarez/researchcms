<?php

namespace App\Controller;

use App\Entity\Authorship;
use App\Form\AuthorshipType;
use App\Repository\AuthorshipRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */


/**
 * @Route("/{_locale}/authorship", defaults = {"_locale" = "en"})
 */
class AuthorshipController extends Controller
{
    /**
     * @Route("/", name="authorship_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(AuthorshipRepository $authorshipRepository): Response
    {
        return $this->render('authorship/index.html.twig', ['authorships' => $authorshipRepository->findAll()]);
    }

    /**
     * @Route("/new", name="authorship_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $authorship = new Authorship();
        $form = $this->createForm(AuthorshipType::class, $authorship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (is_null($authorship->getMember()) OR is_null($authorship->getPublication()) OR is_null($authorship->getTypeOfAuthorship())){}
            else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($authorship);
                $em->flush();

                return $this->redirectToRoute('authorship_index');
            }
        }

        return $this->render('authorship/new.html.twig', [
            'authorship' => $authorship,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="authorship_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Authorship $authorship): Response
    {
        return $this->render('authorship/show.html.twig', ['authorship' => $authorship]);
    }

    /**
     * @Route("/{id}/edit", name="authorship_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Authorship $authorship): Response
    {
        $form = $this->createForm(AuthorshipType::class, $authorship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('authorship_index');

            //return $this->redirectToRoute('authorship_edit', ['id' => $authorship->getId()]);
        }

        return $this->render('authorship/edit.html.twig', [
            'authorship' => $authorship,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="authorship_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Authorship $authorship): Response
    {
        if ($this->isCsrfTokenValid('delete'.$authorship->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($authorship);
            $em->flush();
        }

        return $this->redirectToRoute('authorship_index');
    }
}
