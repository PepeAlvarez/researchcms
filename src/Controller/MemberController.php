<?php

namespace App\Controller;

use App\Entity\Member;
use App\Form\MemberType;
use App\Entity\Authorship;
use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Publication;
use App\Repository\PublicationRepository;
use Unirest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Utils\Accents;
use Symfony\Component\Translation\TranslatorInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/member", defaults = {"_locale" = "en"})
 */
class MemberController extends Controller
{
    /**
     * @Route("/pub", name="member_pub", methods="GET")
     */
    public function pub(MemberRepository $memberRepository): Response
    {
        return $this->render('member/pub.html.twig', ['members' => $memberRepository->findAllExceptAdmin('admin')]);
    }

    /**
     * @Route("/pub/{id}", name="member_pub_show", methods="GET")
     */
    public function pubshow(Member $member, Request $request, MemberRepository $memberRepository): Response
    {
        $member_id = $request->attributes->get('_route_params')['id'];
        $member = $memberRepository->findOneBy(['id' => $member_id]);
        if ($member->getUsername() == "admin" OR $member == null)
            return $this->redirectToRoute('app_start_index');
        else
            return $this->render('member/pubshow.html.twig', ['member' => $member]);
    }

    /**
     * @Route("/", name="member_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(MemberRepository $memberRepository): Response
    {
        return $this->render('member/index.html.twig', ['members' => $memberRepository->findAll()]);
    }

    /**
     * @Route("/new", name="member_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator): Response
    {
        //$roles = $this->getParameter('security.role_hierarchy.roles');
        $member = new Member();
        $form = $this->createForm(MemberType::class, $member);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {

                if ($member->getPlainPassword() != null) {
                    $password = $passwordEncoder->encodePassword($member, $member->getPlainPassword());
                    $member->setPassword($password);
                }

                if ($_FILES['member']['size']['pictureProfile'] > 0) {
                    $image = fread(fopen($_FILES['member']['tmp_name']['pictureProfile'], 'r'), filesize($_FILES['member']['tmp_name']['pictureProfile']));
                    //$image = mb_check_encoding($image, 'UTF-8') ? $image : utf8_encode($image);
                    $member->setPictureProfile($image);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($member);
                $em->flush();

                if (!is_null($member->getOrcidId()) AND $form->get("orcidCheckbox")->getData()) {
                    $this->orcid($member);
                }

                return $this->redirectToRoute('member_index');

            } catch (\Doctrine\DBAL\DBALException $exception) {

                ////////////////////////
                //TODO
                //Enable Flash messages
                ////////////////////////
                $this->get('session')->getFlashBag()->add('error', $translator->trans('Can\'t insert entity.'));

                return $this->redirectToRoute('member_index');
            }


        }

        return $this->render('member/new.html.twig', [
            'member' => $member,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="member_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Member $member): Response
    {
        /*
        if ($member->getPictureProfile() != null) {
            $image = base64_encode(stream_get_contents($member->getPictureProfile()));
        } else {
            $image = null;
        }
        */
        //return $this->render('member/show.html.twig', ['member' => $member, 'image' => $image]);
        return $this->render('member/show.html.twig', ['member' => $member]);
    }


    /**
     * @Route("/{id}/editself", name="member_edit_self", methods="GET|POST")
     * @Security("has_role('ROLE_USER')")
     */
    public function edit_self($id, Request $request, Member $member, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        //$roles = $this->getParameter('security.role_hierarchy.roles');

        $user_id = $this->getUser()->getId();

        // get user from database
        $em = $this->getDoctrine()->getManager();
        $member = $em->getRepository(Member::class)->find($id);

        // user doesn't exist
        if (!$member) {
            throw $this->createNotFoundException($translator->trans('No member found for id ') . $id);
        }

        if ($user_id != $id) {
            throw $this->createAccessDeniedException($translator->trans('You are not allowed to edit other members'));
        }

        $originalPassword = $member->getPassword();    # encoded password

        $form = $this->createForm(MemberType::class, $member);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encode the password if needed
            # password has changed
            if (!empty($form->get('plainPassword')->getData())) {
                //$password = $this->get('security.password_encoder')
                //    ->encodePassword($member, $member->getPlainPassword());
                $password = $passwordEncoder->encodePassword($member, $member->getPlainPassword());
                $member->setPassword($password);
                # password not changed
            } else {
                $member->setPassword($originalPassword);
            }

            if ($_FILES['member']['size']['pictureProfile'] > 0) {
                $image = fread(fopen($_FILES['member']['tmp_name']['pictureProfile'], 'r'), filesize($_FILES['member']['tmp_name']['pictureProfile']));
                $member->setPictureProfile($image);
            } else {
                //We keep the previous image if no file has been selected.
                //This is an ugly solution, because we have to deal directly with the database
                //but no other solution was found.
                $sql = "SELECT picture_profile FROM members WHERE id = :id";
                $params['id'] = $member->getId();
                $em = $this->getDoctrine()->getManager();
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute($params);
                $member->setPictureProfile($stmt->fetchColumn(0));
            }

            $this->getDoctrine()->getManager()->flush();

            if (!is_null($member->getOrcidId()) AND $form->get("orcidCheckbox")->getData()) {
                $this->orcid($member);
            }

            //return $this->redirectToRoute('member_edit', ['id' => $member->getId()]);

            if ($this->isGranted("ROLE_ADMIN"))
                return $this->redirectToRoute('member_index');
            else
                return $this->redirectToRoute('member_pub');

        }

        return $this->render('member/edit.html.twig', [
            'member' => $member,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="member_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit($id, Request $request, Member $member, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $translator): Response
    {
        //$roles = $this->getParameter('security.role_hierarchy.roles');

        // get user from database
        $em = $this->getDoctrine()->getManager();
        $member = $em->getRepository(Member::class)->find($id);

        // user doesn't exist
        if (!$member) {
            throw $this->createNotFoundException($translator->trans('No member found for id ') . $id);
        }

        // build the form with user data
        $originalPassword = $member->getPassword();    # encoded password

        $form = $this->createForm(MemberType::class, $member);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Encode the password if needed
            # password has changed
            if (!empty($form->get('plainPassword')->getData())) {
                //$password = $this->get('security.password_encoder')
                //    ->encodePassword($member, $member->getPlainPassword());
                $password = $passwordEncoder->encodePassword($member, $member->getPlainPassword());
                $member->setPassword($password);

                # password not changed
            } else {
                $member->setPassword($originalPassword);
            }

            if ($_FILES['member']['size']['pictureProfile'] > 0) {
                $image = fread(fopen($_FILES['member']['tmp_name']['pictureProfile'], 'r'), filesize($_FILES['member']['tmp_name']['pictureProfile']));
                $member->setPictureProfile($image);
            } else {
                //We keep the previous image if no file has been selected.
                //This is an ugly solution, because we have to deal directly with the database
                //but no other solution was found.
                $sql = "SELECT picture_profile FROM members WHERE id = :id";
                $params['id'] = $member->getId();
                $em = $this->getDoctrine()->getManager();
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute($params);
                $member->setPictureProfile($stmt->fetchColumn(0));
            }

            $this->getDoctrine()->getManager()->flush();

            if (!is_null($member->getOrcidId()) AND $form->get("orcidCheckbox")->getData()) {
                $this->orcid($member);
            }

            //return $this->redirectToRoute('member_edit', ['id' => $member->getId()]);
            return $this->redirectToRoute('member_index');
        }

        return $this->render('member/edit.html.twig', [
            'member' => $member,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="member_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Member $member): Response
    {
        if ($this->isCsrfTokenValid('delete' . $member->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($member);
            $em->flush();
        }

        return $this->redirectToRoute('member_index');
    }


    private function orcid(Member $member)
    {

// +"access_token": "3fd8967e-89ec-450b-a060-35f566576fcf"
//  +"token_type": "bearer"
//  +"refresh_token": "6e4f84a4-47d5-49e4-95a2-3905d50bd6be"
//  +"expires_in": 631138518
//  +"scope": "/read-public"
//  +"orcid": null

        $access_token = "d49862d4-4722-44bc-b32e-6bafcfc85c01";

        //$headers = array('Accept' => 'application/vnd.orcid+xml', 'Authorization' => 'Bearer', 'Access token' => '3fd8967e-89ec-450b-a060-35f566576fcf');
        $headers = array("Accept" => "application/json", "Authorization" => "Bearer", "Access token" => $access_token);

        //$body = 'client_id=APP-XJUJZ0H19IOLUQ74&client_secret=aaf1ccca-a413-473d-8097-0086becb9422&grant_type=client_credentials&scope=/read-public';

        //$response = Unirest\Request::post('https://sandbox.orcid.org/oauth/token',$headers, $body);
        $mainAddress = "https://pub.orcid.org";
        $apiVersion = "v2.1";
        //$orcidID = "0000-0002-1206-1084";
        $orcidID = $member->getOrcidId();
        $query = "record";
        //$url = 'https://pub.sandbox.orcid.org/v2.1/0000-0002-9284-9954/works';
        $url = $mainAddress . "/" . $apiVersion . "/" . $orcidID . "/" . $query;

        $response = Unirest\Request::get($url, $headers);

        $author_surname = Accents::remove_accents($response->body->person->name->{"family-name"}->value);
        $auhor_firstname = Accents::remove_accents($response->body->person->name->{"given-names"}->value);
        $author_fullname1 = $auhor_firstname . " " . $author_surname;
        $author_fullname2 = $author_surname . ", " . $auhor_firstname;
        $author_fullname3 = $author_surname . ", " . strtoupper($auhor_firstname[0]);

        $authorship_type = "author";

        $em = $this->getDoctrine()->getManager();

        foreach ($response->body->{"activities-summary"}->works->group as $pub) {

            $query = $pub->{"work-summary"}[0]->path;
            $url = $mainAddress . '/' . $apiVersion . $query;

            $work_petition = Unirest\Request::get($url, $headers);

            $work_response = $work_petition->body;

            $publication = new Publication();

            $publication->setTitle($work_response->title->title->value);

            $publication->setType(str_replace('_', '-', strtolower($work_response->type)));

            if (property_exists($work_response, "publication-date")) {
                if (!is_null($work_response->{"publication-date"})) {
                    if (!is_null($work_response->{"publication-date"}->year)) {
                        $publication->setYear($work_response->{"publication-date"}->year->value);
                        if (!is_null($work_response->{"publication-date"}->month) && !is_null($work_response->{"publication-date"}->day)) {
                            $pub_date = \DateTime::createFromFormat('Y-m-d', ($work_response->{"publication-date"}->year->value . "-" . $work_response->{"publication-date"}->month->value . "-" . $work_response->{"publication-date"}->day->value));
                            $publication->setDate($pub_date);
                        }
                    }
                }
            }

            if (property_exists($work_response, "short-description")) {
                if (!is_null($work_response->{"short-description"})) {
                    $publication->setAbstract($work_response->{"short-description"}->value);
                }
            }

            if (!is_null($work_response->{"external-ids"})) {
                foreach ($work_response->{"external-ids"}->{"external-id"} as $external_id) {
                    if ($external_id->{"external-id-type"} == "doi") {
                        //$publication->setDoi($external_id->{"external-id-value"});
                        $publication->setDoi(preg_replace("/(https?:\/\/(dx\.)?doi.org\/)(.*)/", "$3", $external_id->{"external-id-value"}));
                    }
                    if ($external_id->{"external-id-type"} == "issn") {
                        $publication->setIssn($external_id->{"external-id-value"});
                    }
                    if ($external_id->{"external-id-type"} == "eid") {
                        $publication->setEid($external_id->{"external-id-value"});
                    }
                    if ($external_id->{"external-id-type"} == "isbn") {
                        $publication->setEid($external_id->{"external-id-value"});
                    }
                }
            }

            if (!is_null($work_response->url)) {
                $publication->setUrl($work_response->url->value);
            }

            if (!is_null($work_response->contributors)) {

                foreach ($work_response->contributors->contributor as $contributor) {

                    if (!is_null($contributor->{"credit-name"})) {

                        $contributor_name = Accents::remove_accents($contributor->{"credit-name"}->value);

                        if ((strpos($contributor_name, $author_fullname1) !== false) OR (strpos($contributor_name, $author_fullname2) !== false) OR (strpos($contributor_name, $author_fullname3) !== false)) {

                            if (!is_null($contributor->{"contributor-attributes"})) {

                                if (!is_null($contributor->{"contributor-attributes"}->{"contributor-role"})) {

                                    $authorship_type = str_replace('_', '-', strtolower($contributor->{"contributor-attributes"}->{"contributor-role"}));

                                }
                            }

                        }
                    }
                }
            }

            if (property_exists($work_response, "journal-title")) {
                if (!is_null($work_response->{"journal-title"})) {
                    $publication->setJournal($work_response->{"journal-title"}->value);
                }
            }


            if (property_exists($work_response, "citation")) {
                if (!is_null($work_response->citation)) {
                    if ($work_response->citation->{"citation-type"} == "BIBTEX") {

                        $publication->setBibTex($work_response->citation->{"citation-value"});

                        $bibtex = $publication->getBibTex();

                        //Eliminate special characters in bibtex
                        $bibtex = preg_replace("/\{\\\\'\{?\\\?(\w)\}\}?/", "$1", $bibtex);
                        $bibtex = preg_replace('/\{\\\\"\{?\\\?(\w)\}\}?/', "$1", $bibtex);

                        //$publication->setBibTex($bibtex);

                        if (preg_match("/(?<=title\s=\s\{|title=\s\{|title=\{|title\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getTitle()))
                            $publication->setTitle($matches[0]);

                        if (preg_match("/(?<=journal\s=\s\{|journal=\s\{|journal=\{|journal\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getJournal()))
                            $publication->setJournal($matches[0]);

                        if (preg_match("/(?<=year\s=\s\{|year=\s\{|year=\{|year\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getYear()))
                            $publication->setYear(intval($matches[0]));

                        if (preg_match("/(?<=issn\s=\s\{|issn=\s\{|issn=\{|issn\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getIssn()))
                            $publication->setIssn($matches[0]);

                        if (preg_match("/(?<=isbn\s=\s\{|isbn=\s\{|isbn=\{|isbn\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getIsbn()))
                            $publication->setIsbn($matches[0]);

                        if (preg_match("/(?<=eid\s=\s\{|eid=\s\{|eid=\{|eid\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getEid()))
                            $publication->setEid($matches[0]);

                        if (preg_match("/(?<=doi\s=\s\{|doi=\s\{|doi=\{|doi\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getDoi()))
                            //$publication->setDoi($matches[0]);
                            $publication->setDoi(preg_replace("/(https?:\/\/(dx\.)?doi.org\/)(.*)/", "$3", $matches[0]));

                        if (preg_match("/(?<=abstract\s=\s\{|abstract=\s\{|abstract=\{|abstract\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getAbstract()))
                            $publication->setAbstract($matches[0]);

                        //We look for the string "volume = {xxxx}" and fetch the "xxxx"
                        if (preg_match("/(?<=volume\s=\s\{|volume=\s\{|volume=\{|volume\s=\{)[^\}]*(?=\})/", $bibtex, $matches))
                            $publication->setVolume(intval($matches[0]));

                        //We look for the string "number = {xxxx}" and fetch the "xxxx"
                        if (preg_match("/(?<=number\s=\s\{|number=\s\{|number=\{|number\s=\{)[^\}]*(?=\})/", $bibtex, $matches))
                            $publication->setNumber(intval($matches[0]));

                        //We look for the string "pages = {xxxx-xxxx}" and fetch the "xxxx-xxxx"
                        if (preg_match("/(?<=pages\s=\s\{|pages=\s\{|pages=\{|pages\s=\{)[^\}]*(?=\})/", $bibtex, $matches))
                            $publication->setPages($matches[0]);

                        //We look for the string "author = {Xxxxx, X. and Xxxxx, X.}" and fetch the "Xxxxx, X. and Xxxxx, X."
                        if (preg_match("/(?<=author\s=\s\{|author=\s\{|author=\{|author\s=\{)[^\}]*(?=\})/", $bibtex, $matches))
                            $publication->setAuthors($matches[0]);

                        //We look for the string "keywords = {Xxxxx, Xxxxx, Xxxx}" and fetch the "Xxxxx, Xxxxx, Xxxx"
                        if (preg_match("/(?<=keywords\s=\s\{|keywords=\s\{|keywords=\{|keywords\s=\{)[^\}]*(?=\})/", $bibtex, $matches))
                            $publication->setKeywords($matches[0]);

                        //We look for the string "pubstate = {Xxxxx}" and fetch the "Xxxxx"
                        if (preg_match("/(?<=pubstate\s=\s\{|pubstate=\s\{|pubstate=\{|pubstate\s=\{)[^\}]*(?=\})/", $bibtex, $matches))
                            $publication->setPubstate($matches[0]);

                        if (preg_match("/(?<=url\s=\s\{|url=\s\{|url=\{|url\s=\{)[^\}]*(?=\})/", $bibtex, $matches) AND is_null($publication->getUrl()))
                            $publication->setUrl($matches[0]);
                        elseif (!is_null($publication->getDoi()))
                            $publication->setUrl("https://doi.org/" . $publication->getDoi());
                    }
                }
            }

            //We couldn't get the authors from the bibTex, so we will try to fetch them from the retrieved data
            if (is_null($publication->getAuthors())) {

                $contributorsarray = [];

                if (!is_null($work_response->contributors)) {

                    foreach ($work_response->contributors->contributor as $contributor) {

                        if (!is_null($contributor->{"credit-name"})) {

                            $contributorsarray [] = $contributor->{"credit-name"}->value;
                        }
                    }

                    $contributors = implode(" and ", $contributorsarray);

                    $publication->setAuthors($contributors);
                }

            }

            //If untill this point we haven't got the authors, we assign the actual member as author
            if (is_null($publication->getAuthors())) {
                $publication->setAuthors($author_fullname2);
            }

            //Let's check if this publication exists.
            $publication_found = $this->getDoctrine()->getRepository(Publication::class)->findOneBy([
                "title" => $publication->getTitle(),
                "type" => $publication->getType(),
                "year" => $publication->getYear()
            ]);

            if (is_null($publication_found)) {

                $em->persist($publication);
                $em->flush($publication);

            }

            //Let's check if this member is already registered as an author of this publication.
            $publication_found = $this->getDoctrine()->getRepository(Publication::class)->findOneBy([
                "title" => $publication->getTitle(),
                "type" => $publication->getType(),
                "year" => $publication->getYear()
            ]);

            $authorship_found = $this->getDoctrine()->getRepository(Authorship::class)->findOneBy([
                "member" => $member,
                "publication" => $publication_found
            ]);

            if (is_null($authorship_found)) {

                $authorship = new Authorship();
                $authorship->setMember($member);
                $authorship->setPublication($publication_found);
                $authorship->setTypeOfAuthorship($authorship_type);
                $em->persist($authorship);
                $em->flush($authorship);

            }

        }

        return;
    }
}
