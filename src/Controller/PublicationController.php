<?php

namespace App\Controller;

use App\Entity\Publication;
use App\Entity\Authorship;
use App\Form\PublicationType;
use App\Repository\PublicationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Translation\TranslatorInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/publication", defaults = {"_locale" = "en"})
 */
class PublicationController extends Controller
{

    /**
     * @Route("/pub/{currentPage}/{limit}", name="publication_pub", methods="GET", defaults={"currentPage" = 1, "limit" = 4}, requirements={"currentPage"="\d+"})
     */
    public function pub(PublicationRepository $publicationRepository, $currentPage, $limit, TranslatorInterface $translator): Response
    {
        // ... get posts from DB...
        // Controller Action

        //$limit = 4;
        $publications = $publicationRepository->getAllPublications($currentPage, $limit);

        $years = array();
        foreach ($publications as $p)
        {
            if (!in_array($p->getYear(), $years))
            {
                $years[] = $p->getYear();
            }
        }

        rsort($years);

        $publications_count = $publications->count();
        if ($limit == 0) $limit = 1;
        $maxPages = intval(ceil($publications_count / $limit));
        $thisPage = $currentPage;
        $show_all = true;

        if($publications_count == $limit)
            $show_all = false;

        if (!$publications) {
            throw $this->createNotFoundException($translator->trans('Unable to find publications'));
        }
        // Pass through the 3 above variables to calculate pages in twig
        return $this->render('publication/pub.html.twig', array(
            'publications' => $publications,
            'maxPages' => $maxPages,
            'thisPage' => $thisPage,
            'years' => $years,
            'limit' => $limit,
            'count' => $publications_count,
            'showAll' => $show_all
        ));
    }

//        /**
//     * @Route("/pub", name="publication_pub", methods="GET")
//     */
/*    public function pub(PublicationRepository $publicationRepository): Response
    {
        $publications = $publicationRepository->findAll();

        $years = array();
        foreach ($publications as $p)
        {
            if (!in_array($p->getYear(), $years))
            {
                $years[] = $p->getYear();
            }
        }

        rsort($years);

        return $this->render('publication/pub.html.twig', ['publications' => $publications,'years' => $years]);
    }*/


    /**
     * @Route("/pub/show/{id}", name="publication_pub_show", methods="GET")
     */
    public function pubshow(Publication $publication, Request $request, PublicationRepository $publicationRepository): Response
    {
        $publication_id = $request->attributes->get('_route_params')['id'];
        $publication = $publicationRepository->findOneBy(['id' => $publication_id]);

        return $this->render('publication/pubshow.html.twig', ['publication' => $publication]);
    }

    /**
     * @Route("/", name="publication_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(PublicationRepository $publicationRepository): Response
    {


        return $this->render('publication/index.html.twig', ['publications' => $publicationRepository->findAll()]);
    }

    /**
     * @Route("/new", name="publication_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request, TranslatorInterface $translator): Response
    {
        $publication = new Publication();
        $form = $this->createForm(PublicationType::class, $publication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($publication);
                $em->flush();

                return $this->redirectToRoute('publication_index');
            } catch (\Doctrine\DBAL\DBALException $exception) {

                ////////////////////////
                //TODO
                //Enable Flash messages
                ////////////////////////
                $this->get('session')->getFlashBag()->add('error', $translator->trans('Can\'t insert entity.'));

                return $this->redirectToRoute('publication_index');
            }
        }

        return $this->render('publication/new.html.twig', [
            'publication' => $publication,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="publication_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Publication $publication): Response
    {
        return $this->render('publication/show.html.twig', ['publication' => $publication]);
    }

    /**
     * @Route("/{id}/edit", name="publication_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Publication $publication): Response
    {
        $form = $this->createForm(PublicationType::class, $publication);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('publication_edit', ['id' => $publication->getId()]);
            return $this->redirectToRoute('publication_index');
        }

        return $this->render('publication/edit.html.twig', [
            'publication' => $publication,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="publication_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Publication $publication): Response
    {
        if ($this->isCsrfTokenValid('delete' . $publication->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($publication);
            $em->flush();
        }

        return $this->redirectToRoute('publication_index');
    }
}
