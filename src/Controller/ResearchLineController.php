<?php

namespace App\Controller;

use App\Entity\ResearchLine;
use App\Form\ResearchLineType;
use App\Repository\ResearchLineRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/research/line", defaults = {"_locale" = "en"})
 */
class ResearchLineController extends Controller
{
    /**
     * @Route("/", name="research_line_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(ResearchLineRepository $researchLineRepository): Response
    {
        return $this->render('research_line/index.html.twig', ['research_lines' => $researchLineRepository->findAll()]);
    }

    /**
     * @Route("/new", name="research_line_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $researchLine = new ResearchLine();
        $form = $this->createForm(ResearchLineType::class, $researchLine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($researchLine);
            $em->flush();

            return $this->redirectToRoute('research_line_index');
        }

        return $this->render('research_line/new.html.twig', [
            'research_line' => $researchLine,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="research_line_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(ResearchLine $researchLine): Response
    {
        return $this->render('research_line/show.html.twig', ['research_line' => $researchLine]);
    }

    /**
     * @Route("/{id}/edit", name="research_line_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, ResearchLine $researchLine): Response
    {
        $form = $this->createForm(ResearchLineType::class, $researchLine);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            //return $this->redirectToRoute('research_line_edit', ['id' => $researchLine->getId()]);
            return $this->redirectToRoute('research_line_index');
        }

        return $this->render('research_line/edit.html.twig', [
            'research_line' => $researchLine,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="research_line_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, ResearchLine $researchLine): Response
    {
        if ($this->isCsrfTokenValid('delete'.$researchLine->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($researchLine);
            $em->flush();
        }

        return $this->redirectToRoute('research_line_index');
    }
}
