<?php

namespace App\Controller;

use App\Entity\ResearchProject;
use App\Form\ResearchProjectType;
use App\Repository\ResearchProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Translation\TranslatorInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/research/project", defaults = {"_locale" = "en"})
 */
class ResearchProjectController extends Controller
{
    /**
     * @Route("/pub", name="research_project_pub", methods="GET")
     */
    public function pub(ResearchProjectRepository $researchProjectRepository): Response
    {
        //return $this->render('research_project/pub.html.twig', ['research_projects' => $researchProjectRepository->findAll()]);
        return $this->render('research_project/pub.html.twig', ['research_projects' => $researchProjectRepository->findBy(array(),array('id' => 'DESC'))]);
    }

    /**
     * @Route("/pub/{id}", name="research_project_pub_show", methods="GET")
     */
    public function pubshow(ResearchProject $researchProject, Request $request, ResearchProjectRepository $researchProjectRepository): Response
    {
        $researchProject_id = $request->attributes->get('_route_params')['id'];
        $researchProject = $researchProjectRepository->findOneBy(['id' => $researchProject_id]);

        return $this->render('research_project/pubshow.html.twig', ['research_project' => $researchProject]);
    }

    /**
     * @Route("/", name="research_project_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(ResearchProjectRepository $researchProjectRepository): Response
    {
        return $this->render('research_project/index.html.twig', ['research_projects' => $researchProjectRepository->findAll()]);
        //return $this->render('research_project/index.html.twig', ['research_projects' => $researchProjectRepository->findBy(array(), array('id' => 'ASC'))]);
    }

    /**
     * @Route("/new", name="research_project_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $researchProject = new ResearchProject();
        $form = $this->createForm(ResearchProjectType::class, $researchProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($_FILES['research_project']['size']['picture1'] > 0) {
                $image = fread(fopen($_FILES['research_project']['tmp_name']['picture1'], 'r'), filesize($_FILES['research_project']['tmp_name']['picture1']));
                //$image = mb_check_encoding($image, 'UTF-8') ? $image : utf8_encode($image);
                $researchProject->setPicture1($image);
            }

            if ($_FILES['research_project']['size']['picture2'] > 0) {
                $image = fread(fopen($_FILES['research_project']['tmp_name']['picture2'], 'r'), filesize($_FILES['research_project']['tmp_name']['picture2']));
                //$image = mb_check_encoding($image, 'UTF-8') ? $image : utf8_encode($image);
                $researchProject->setPicture2($image);
            }

            if ($_FILES['research_project']['size']['picture3'] > 0) {
                $image = fread(fopen($_FILES['research_project']['tmp_name']['picture3'], 'r'), filesize($_FILES['research_project']['tmp_name']['picture3']));
                //$image = mb_check_encoding($image, 'UTF-8') ? $image : utf8_encode($image);
                $researchProject->setPicture3($image);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($researchProject);
            $em->flush();

            return $this->redirectToRoute('research_project_index');
        }

        return $this->render('research_project/new.html.twig', [
            'research_project' => $researchProject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="research_project_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(ResearchProject $researchProject): Response
    {
        return $this->render('research_project/show.html.twig', ['research_project' => $researchProject]);
    }

    /**
     * @Route("/{id}/edit", name="research_project_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, ResearchProject $researchProject, TranslatorInterface $translator): Response
    {
        $form = $this->createForm(ResearchProjectType::class, $researchProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            try {


                if ($_FILES['research_project']['size']['picture1'] > 0) {
                    $image = fread(fopen($_FILES['research_project']['tmp_name']['picture1'], 'r'), filesize($_FILES['research_project']['tmp_name']['picture1']));
                    $researchProject->setPicture1($image);
                } else {
                    //We keep the previous image if no file has been selected.
                    //This is an ugly solution, because we have to deal directly with the database
                    //but no other solution was found.
                    $sql = "SELECT picture1 FROM researchprojects WHERE id = :id";
                    $params['id'] = $researchProject->getId();
                    $em = $this->getDoctrine()->getManager();
                    $stmt = $em->getConnection()->prepare($sql);
                    $stmt->execute($params);
                    $researchProject->setPicture1($stmt->fetchColumn(0));
                }

                if ($_FILES['research_project']['size']['picture2'] > 0) {
                    $image = fread(fopen($_FILES['research_project']['tmp_name']['picture2'], 'r'), filesize($_FILES['research_project']['tmp_name']['picture2']));
                    $researchProject->setPicture2($image);
                } else {
                    //We keep the previous image if no file has been selected.
                    //This is an ugly solution, because we have to deal directly with the database
                    //but no other solution was found.
                    $sql = "SELECT picture2 FROM researchprojects WHERE id = :id";
                    $params['id'] = $researchProject->getId();
                    $em = $this->getDoctrine()->getManager();
                    $stmt = $em->getConnection()->prepare($sql);
                    $stmt->execute($params);
                    $researchProject->setPicture2($stmt->fetchColumn(0));
                }

                if ($_FILES['research_project']['size']['picture3'] > 0) {
                    $image = fread(fopen($_FILES['research_project']['tmp_name']['picture3'], 'r'), filesize($_FILES['research_project']['tmp_name']['picture3']));
                    $researchProject->setPicture3($image);
                } else {
                    //We keep the previous image if no file has been selected.
                    //This is an ugly solution, because we have to deal directly with the database
                    //but no other solution was found.
                    $sql = "SELECT picture3 FROM researchprojects WHERE id = :id";
                    $params['id'] = $researchProject->getId();
                    $em = $this->getDoctrine()->getManager();
                    $stmt = $em->getConnection()->prepare($sql);
                    $stmt->execute($params);
                    $researchProject->setPicture3($stmt->fetchColumn(0));
                }

                $this->getDoctrine()->getManager()->flush();

                //return $this->redirectToRoute('research_project_edit', ['id' => $researchProject->getId()]);
                return $this->redirectToRoute('research_project_index');
            } catch (\Doctrine\DBAL\DBALException $exception) {

                ////////////////////////
                //TODO
                //Enable Flash messages
                ////////////////////////
                $this->get('session')->getFlashBag()->add('error', $translator->trans('Can\'t insert entity.'));

                return $this->redirectToRoute('research_project_index');
            }
        }

        return $this->render('research_project/edit.html.twig', [
            'research_project' => $researchProject,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="research_project_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, ResearchProject $researchProject): Response
    {
        if ($this->isCsrfTokenValid('delete' . $researchProject->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($researchProject);
            $em->flush();
        }

        return $this->redirectToRoute('research_project_index');
    }
}
