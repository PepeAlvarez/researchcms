<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\Slugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Translation\TranslatorInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/post", defaults = {"_locale" = "en"})
 */
class PostController extends AbstractController
{

 //   /**
 //    * @Route("/pub/{currentPage}", name="post_pub", methods="GET", defaults={"currentPage" = 1})
 //    */
    /**
     * @Route("/pub/{currentPage}/{limit}", name="post_pub", methods="GET", defaults={"currentPage" = 1, "limit" = 4}, requirements={"currentPage"="\d+"})
     */

    public function pub(PostRepository $postRepository, $currentPage, $limit, TranslatorInterface $translator): Response
    {
        // ... get posts from DB...
        // Controller Action

        //$limit = 5;
        $posts = $postRepository->getAllPosts($currentPage, $limit);

        $posts_count = $posts->count();
        if ($limit == 0) $limit = 1;
        $maxPages = intval(ceil($posts_count/$limit));
        $thisPage = $currentPage;

        $show_all = true;

        if($posts_count == $limit)
            $show_all = false;

        if (!$posts) {
            throw $this->createNotFoundException($translator->trans('Unable to find posts'));
        }
        // Pass through the 3 above variables to calculate pages in twig
        return $this->render('post/pub.html.twig',array(
            'posts' => $posts,
            'maxPages'=>$maxPages,
            'thisPage' => $thisPage,
            'limit' => $limit,
            'count' => $posts_count,
            'showAll' => $show_all
        ) );


       // return $this->render('post/pub.html.twig', ['posts' => $postRepository->findBy(array(), array('publishedAt' => 'DESC','id' => 'DESC'))]);
    }

    /*public function pub(PostRepository $postRepository): Response
    {
        return $this->render('post/pub.html.twig', ['posts' => $postRepository->findBy(array(), array('publishedAt' => 'DESC','id' => 'DESC'))]);
    }*/

    /**
     * @Route("/pub/show/{slug}", name="post_pub_show", methods="GET")
     */
    public function pubshow(Post $post, PostRepository $postRepository): Response
    {
        return $this->render('post/pubshow.html.twig', ['post' => $post, 'previous' => $postRepository->findOneByPreviousDate($post->getPublishedAt()), 'next' => $postRepository->findOneByNextDate($post->getPublishedAt())]);
    }


    /**
     * @Route("/", name="post_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('post/index.html.twig', ['posts' => $postRepository->findAll()]);
    }

    /**
     * @Route("/new", name="post_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setSlug(Slugger::slugify($post->getTitle()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Post $post): Response
    {
        return $this->render('post/show.html.twig', ['post' => $post]);
    }

    /**
     * @Route("/{id}/edit", name="post_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Post $post): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setSlug(Slugger::slugify($post->getTitle()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('post_index');
            //return $this->redirectToRoute('post_edit', ['id' => $post->getId()]);
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($post);
            $em->flush();
        }

        return $this->redirectToRoute('post_index');
    }
}
