<?php

namespace App\Controller;

use App\Entity\Home;
use App\Form\HomeType;
use App\Repository\HomeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @Route("/{_locale}/home", defaults = {"_locale" = "en"})
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="home_index", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function index(HomeRepository $homeRepository): Response
    {
        return $this->render('home/index.html.twig', ['homes' => $homeRepository->findAll()]);
    }

    /**
     * @Route("/new", name="home_new", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function new(Request $request): Response
    {
        $home = new Home();
        $form = $this->createForm(HomeType::class, $home);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($_FILES['home']['size']['logo'] > 0) {
                $image = fread(fopen($_FILES['home']['tmp_name']['logo'], 'r'), filesize($_FILES['home']['tmp_name']['logo']));
                //$image = mb_check_encoding($image, 'UTF-8') ? $image : utf8_encode($image);
                $home->setLogo($image);
            }


            $em = $this->getDoctrine()->getManager();
            $em->persist($home);
            $em->flush();

            return $this->redirectToRoute('home_index');
        }

        return $this->render('home/new.html.twig', [
            'home' => $home,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_show", methods="GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function show(Home $home): Response
    {
        return $this->render('home/show.html.twig', ['home' => $home]);
    }

    /**
     * @Route("/{id}/edit", name="home_edit", methods="GET|POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function edit(Request $request, Home $home): Response
    {
        $form = $this->createForm(HomeType::class, $home);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($_FILES['home']['size']['logo'] > 0) {
                $image = fread(fopen($_FILES['home']['tmp_name']['logo'], 'r'), filesize($_FILES['home']['tmp_name']['logo']));
                $home->setLogo($image);
            } else {
                //We keep the previous image if no file has been selected.
                //This is an ugly solution, because we have to deal directly with the database
                //but no other solution was found.
                $sql = "SELECT logo FROM home WHERE id = :id";
                $params['id'] = $home->getId();
                $em = $this->getDoctrine()->getManager();
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute($params);
                $home->setLogo($stmt->fetchColumn(0));
            }



            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_index', ['id' => $home->getId()]);
        }

        return $this->render('home/edit.html.twig', [
            'home' => $home,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="home_delete", methods="DELETE")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function delete(Request $request, Home $home): Response
    {
        if ($this->isCsrfTokenValid('delete'.$home->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($home);
            $em->flush();
        }

        return $this->redirectToRoute('home_index');
    }
}
