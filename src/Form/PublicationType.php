<?php

namespace App\Form;

use App\Entity\Member;
use App\Entity\Publication;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

class PublicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    'Artistic performance' => 'artistic-performance',
                    'Book Chapter' => 'book-chapter',
                    'Book Review' => 'book-review',
                    'Book' => 'book',
                    'Conference Abstract' => 'conference-abstract',
                    'Conference Paper' => 'conference-paper',
                    'Conference Poster' => 'conference-poster',
                    'Data Set' => 'data-set',
                    'Dictionary Entry' => 'dictionary-entry',
                    'Disclosure' => 'disclosure',
                    'Dissertation' => 'dissertation',
                    'Edited Book' => 'edited-book',
                    'Encyclopedia Entry' => 'encyclopedia-entry',
                    'Invention' => 'invention',
                    'Journal Article' => 'journal-article',
                    'Journal Issue' => 'journal-issue',
                    'Lecture Speech' => 'lecture-speech',
                    'License' => 'license',
                    'Magazine Article' => 'magazine-article',
                    'Manual' => 'manual',
                    'Newsletter Article' => 'newsletter-article',
                    'Newspaper Article' => 'newspaper-article',
                    'Online Resource' => 'online-resource',
                    'Other' => 'other',
                    'Patent' => 'patent',
                    'Registered Copyright' => 'registered-copyright',
                    'Report' => 'report',
                    'Research Technique' => 'research-technique',
                    'Research Tool' => 'research-tool',
                    'Spin Off Company' => 'spin-off-company',
                    'Standards And Policy' => 'standards-and-policy',
                    'Supervised Student Publication' => 'supervised-student-publication',
                    'Technical Standard' => 'technical-standard',
                    'Test' => 'test',
                    'Translation' => 'translation',
                    'Trademark' => 'trademark',
                    'Website' => 'website',
                    'Working Paper' => 'working-paper'
                ),
                'placeholder' => 'Select a type',
                'preferred_choices' => array('journal-article'),
                'attr' => array('class' => 'form-control')
            ))
            ->add('title', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('url', TextType::class, array('attr' => array('class' => 'form-control '), 'label' => 'URL'))
            ->add('doi', TextType::class, array('attr' => array('class' => 'form-control'), 'label' => 'DOI'))
            ->add('eid', TextType::class, array('attr' => array('class' => 'form-control'), 'label' => 'EID'))
            ->add('issn', TextType::class, array('attr' => array('class' => 'form-control'), 'label' => 'ISSN'))
            ->add('isbn', TextType::class, array('attr' => array('class' => 'form-control'), 'label' => 'ISBN'))
            ->add('year', IntegerType::class, array('attr' => array('class' => 'form-control')))
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'attr' => array('class' => 'form-control')
            ))
            ->add('journal', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('volume', IntegerType::class, array('attr' => array('class' => 'form-control')))
            ->add('number', IntegerType::class, array('attr' => array('class' => 'form-control')))
            ->add('pages', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('abstract', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('keywords', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('pubstate', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('bibTex', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('authors', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('authorships', CollectionType::class, array(
                'entry_type' => AuthorshipType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Publication::class,
        ]);
    }
}
