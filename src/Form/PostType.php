<?php

namespace App\Form;

use App\Entity\Post;
use App\Form\Type\DateTimePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Member;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'attr' => array('class' => 'form-control','autofocus' => true)
            ))
            ->add('summary', TextareaType::class, array(
                'help' => 'Summaries can\'t contain HTML contents; only plain text.',
                'attr' => array('class' => 'form-control')
            ))
            ->add('content', null, array(
                'attr' => array('class' => 'form-control','rows' => 20),
                'help' => 'HTML is allowed.',
            ))
            ->add('publishedAt', DateType::class, array(
                'widget' => 'single_text',
                'attr' => array('class' => 'form-control'),
                'help' => 'Set the date in the future to schedule the blog post publication.',
                'label' => 'Published at'
            ))
            ->add('author', EntityType::class, array(
                'class' => Member::class,
                'choice_label' => 'getFullName',
                'multiple' => false,
                'attr' => array('class' => 'form-control')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
