<?php

namespace App\Form;

use App\Entity\Collaborator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\ResearchProject;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use PhpParser\Node\Scalar\MagicConst\File;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

class CollaboratorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('lastName', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('email', EmailType::class, array('attr' => array('class' => 'form-control')))
            ->add('phone', TelType::class, array('attr' => array('class' => 'form-control')))
            ->add('researchProjects', EntityType::class, array(
                'by_reference' => false,
                'label' => 'This collaborator collaborates in the following Research Projects (press CTRL key to select more than one)',
                'class' => ResearchProject::class,
                'choice_label' => 'title',
                'multiple' => true,
                'attr' => array('class' => 'form-control')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Collaborator::class,
        ]);
    }
}
