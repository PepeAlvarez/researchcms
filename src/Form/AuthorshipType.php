<?php

namespace App\Form;

use App\Entity\Authorship;
use App\Entity\Publication;
use App\Entity\Member;
use App\Repository\MemberRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

class AuthorshipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('member', EntityType::class, array(
                'class' => Member::class,
                'choice_label' => 'getFullName',
                'multiple' => false,
                'placeholder' => 'Select a member',
                'attr' => array('class' => 'form-control'),
                'query_builder' => function (MemberRepository $memberRepository, $value = "admin") {
                    return $memberRepository->createQueryBuilder('m')
                        ->andWhere('m.username != :val')
                        ->setParameter('val', $value)
                        ->orderBy('m.id', 'ASC');
                }
            ))
            ->add('publication', EntityType::class, array(
                'class' => Publication::class,
                'choice_label' => 'getTitle',
                'multiple' => false,
                'placeholder' => 'Select a publication',
                'attr' => array('class' => 'form-control')
            ))
            ->add('typeOfAuthorship', ChoiceType::class, array(
                    'choices' => array(
                        'Author' => 'author',
                        'Assignee' => 'assignee',
                        'Editor' => 'editor',
                        'Chair or Translator' => 'chair-or-translator',
                        'Co-investigator' => 'co-investigator',
                        'Co-inventor' => 'co-inventor',
                        'Graduate student' => 'graduate-student',
                        'Other inventor' => 'other-inventor',
                        'Principal investigator' => 'principal-investigator',
                        'Postdoctoral researcher' => 'postdoctoral-researcher',
                        'Support staff' => 'support-staff'
                    ),
                    'placeholder' => 'Select a type',
                    'preferred_choices' => array('author'),
                    'attr' => array('class' => 'form-control')
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Authorship::class,
        ]);
    }
}
