<?php

namespace App\Form;

use App\Entity\Collaborator;
use App\Entity\Member;
use App\Repository\MemberRepository;
use App\Entity\ResearchProject;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ResearchProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('reference', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('financialEntity', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('amount', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('participatingEntities', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('duration', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('participantResponsible', EntityType::class, array(
                'class' => Member::class,
                'choice_label' => 'getFullName',
                'multiple' => false,
                'placeholder' => 'Select a member',
                'attr' => array('class' => 'form-control'),
                'query_builder' => function (MemberRepository $memberRepository, $value = "admin") {
                    return $memberRepository->createQueryBuilder('m')
                        ->andWhere('m.username != :val')
                        ->setParameter('val', $value)
                        ->orderBy('m.id', 'ASC');
                }
            ))
            ->add('participants',EntityType::class, array(
                'class' => Member::class,
                'choice_label' => 'getFullName',
                'multiple' => true,
                'attr' => array('class' => 'form-control'),
                'query_builder' => function (MemberRepository $memberRepository, $value = "admin") {
                    return $memberRepository->createQueryBuilder('m')
                        ->andWhere('m.username != :val')
                        ->setParameter('val', $value)
                        ->orderBy('m.id', 'ASC');
                }
                //'expanded' => true
            ))
            ->add('collaborators',EntityType::class, array(
                'class' => Collaborator::class,
                'choice_label' => 'getFullName',
                'multiple' => true,
                'attr' => array('class' => 'form-control')
                //'expanded' => true
            ))
            ->add('unescoCodes', TextareaType::class, array('label' => 'UNESCO Codes', 'attr' => array('class' => 'form-control')))
            ->add('technologicalAreas', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('cnaeCodes', TextareaType::class, array('label' => 'CNAE Codes', 'attr' => array('class' => 'form-control')))
            ->add('picture1', FileType::class, array(
                'data_class' => null,
                'attr' => array('class' => 'form-control'),
            ))
            ->add('picture2', FileType::class, array(
                'data_class' => null,
                'attr' => array('class' => 'form-control'),
            ))
            ->add('picture3', FileType::class, array(
                'data_class' => null,
                'attr' => array('class' => 'form-control'),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ResearchProject::class,
        ]);
    }
}
