<?php

namespace App\Form;

use App\Entity\Member;
use App\Entity\ResearchProject;
use App\Form\Extension\ImageTypeExtension;
use PhpParser\Node\Scalar\MagicConst\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

class MemberType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $roles = $this->getParent('security.role_hierarchy.roles');

        // By default, password is required (create user case)
        $passwordOptions = array(
            'type'           => PasswordType::class,
            'options'=> array('attr' => array('class' => 'form-control')),
            'first_options'  => array('label' => 'Password'),
            'second_options' => array('label' => 'Repeat password'),
            'required'       => true
        );

        // If edit user : password is optional
        // User object is stored in $options['data']
        $recordId = $options['data']->getId();
        if (!empty($recordId)) {
            $passwordOptions['required'] = false;
        }

        $builder
            ->add('firstName', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('lastName', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('pictureProfile', FileType::class, array(
                'data_class' => null,
                'attr' => array('class' => 'form-control'),
                //'empty_data' => '',
                //'image_property' => 'getPictureProfile'
                ))
            ->add('qualifications', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('occupation', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('address', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('email', EmailType::class, array('attr' => array('class' => 'form-control')))
            ->add('phone', TelType::class, array('attr' => array('class' => 'form-control')))
            ->add('fax', TelType::class, array('attr' => array('class' => 'form-control')))
            ->add('subjects', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('membership', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('googleScholarId',TextType::class, array('attr' => array('class' => 'form-control', 'placeholder' => 'xxxxxxxxxxxx')))
            ->add('scopusId', TextType::class, array('attr' => array('class' => 'form-control', 'placeholder' => '0000000000')))
            ->add('mendeleyId', TextType::class,  array('attr' => array('class' => 'form-control', 'placeholder' => 'xxxx-xxxx-xx-xx')))
            ->add('orcidId', TextType::class, array('label' => 'ORCID id','attr' => array('class' => 'form-control', 'placeholder' => '0000-0000-0000-0000')))
            ->add('orcidCheckbox', CheckboxType::class,array('mapped' => false, 'label' => 'Do you want me to import from ORCID.org all the records registered with the previous ID?', 'attr' => array('class' => 'form-control')))
            ->add('researcherId', TextType::class, array('attr' => array('class' => 'form-control', 'placeholder' => 'x-xxxx-xxxx')))
            ->add('hIndex', IntegerType::class, array('attr' => array('class' => 'form-control')))
            ->add('personalWebSite', UrlType::class, array('attr' => array('class' => 'form-control', 'placeholder' => 'wwww.yourwebsite.com')))
            ->add('positions', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('other', TextareaType::class, array('attr' => array('class' => 'form-control')))
            ->add('plainPassword',  RepeatedType::class, $passwordOptions);

        // grab the user, do a quick sanity check that one exists
        $user = $this->security->getUser();
        if (!$user) {
            throw new \LogicException(
                'This section cannot be used without an authenticated user!'
            );
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($user) {

            if(in_array("ROLE_ADMIN", $user->getRoles())) {

                $form = $event->getForm();

                // create the field, this is similar the $builder->add()
                // field name, field type, field options
                $form
                    ->add('username', TextType::class, array('attr' => array('class' => 'form-control')))

                    ->add('isActive', CheckboxType::class, array('attr' => array('class' => 'form-control')))
                    ->add('roles', ChoiceType::class, array(
                            'attr'  =>  array('class' => 'form-control',
                                'style' => 'margin:5px 0;'),
                            'choices' =>
                                array
                                (
                                    'ROLE_ADMIN' => array
                                    (
                                        'Yes' => 'ROLE_ADMIN',
                                    ),
                                    'ROLE_USER' => array
                                    (
                                        'Yes' => 'ROLE_USER'
                                    )
                                )
                        ,
                            'multiple' => true,
                            'required' => true,
                        )
                    )
                    ->add('researchProjectsResponsible', EntityType::class, array(
                        'by_reference' => false,
                        'label' => 'This member is the Leading Researcher in the following Research Projects (press CTRL key to select more than one)',
                        'class' => ResearchProject::class,
                        'choice_label' => 'title',
                        'multiple' => true,
                        'attr' => array('class' => 'form-control')
                    ))
                    ->add('researchProjects', EntityType::class, array(
                        'by_reference' => false,
                        'label' => 'This member participates in the following Research Projects (press CTRL key to select more than one)',
                        'class' => ResearchProject::class,
                        'choice_label' => 'title',
                        'multiple' => true,
                        'attr' => array('class' => 'form-control')
                    ))
                    ->add('authorships', CollectionType::class, array(
                        'entry_type' => AuthorshipType::class,
                        'entry_options' => array('label' => false),
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                    ))
                ;
            }
        });

/*
        $builder
            ->add('username', TextType::class, array('attr' => array('class' => 'form-control')))

            ->add('isActive', CheckboxType::class, array('attr' => array('class' => 'form-control')))
            ->add('roles', ChoiceType::class, array(
                    'attr'  =>  array('class' => 'form-control',
                        'style' => 'margin:5px 0;'),
                    'choices' =>
                        array
                        (
                            'ROLE_ADMIN' => array
                            (
                                'Yes' => 'ROLE_ADMIN',
                            ),
                            'ROLE_USER' => array
                            (
                                'Yes' => 'ROLE_USER'
                            )
                        )
                ,
                    'multiple' => true,
                    'required' => true,
                )
            )
            ->add('researchProjectsResponsible', EntityType::class, array(
                'by_reference' => false,
                'label' => 'This member is the Responsible Researcher in the following Research Projects (press CTRL key to select more than one)',
                'class' => ResearchProject::class,
                'choice_label' => 'title',
                'multiple' => true,
                'attr' => array('class' => 'form-control')
            ))
            ->add('researchProjects', EntityType::class, array(
                'by_reference' => false,
                'label' => 'This member participates in the following Research Projects (press CTRL key to select more than one)',
                'class' => ResearchProject::class,
                'choice_label' => 'title',
                'multiple' => true,
                'attr' => array('class' => 'form-control')
            ))
            ->add('authorships', CollectionType::class, array(
                'entry_type' => AuthorshipType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            ))
        ;*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
