<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180917160649 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE publications (id INT AUTO_INCREMENT NOT NULL, type ENUM(\'artistic-performance\',\'book-chapter\',\'book-review\',\'book\',\'conference-abstract\',\'conference-paper\',\'conference-poster\',\'data-set\',\'dictionary-entry\',\'disclosure\',\'dissertation\',\'edited-book\',\'encyclopedia-entry\',\'invention\',\'journal-article\',\'journal-issue\',\'lecture-speech\',\'license\',\'magazine-article\',\'manual\',\'newsletter-article\',\'newspaper-article\',\'online-resource\',\'other\',\'patent\',\'registered-copyright\',\'report\',\'research-technique\',\'research-tool\',\'spin-off-company\',\'standards-and-policy\',\'supervised-student-publication\',\'technical-standard\',\'test\',\'translation\',\'trademark\',\'website\',\'working-paper\'), title VARCHAR(255) NOT NULL, url VARCHAR(240) DEFAULT NULL, doi VARCHAR(40) DEFAULT NULL, issn VARCHAR(20) DEFAULT NULL, year INT DEFAULT NULL, date DATE DEFAULT NULL, journal VARCHAR(255) DEFAULT NULL, volume INT DEFAULT NULL, pages VARCHAR(20) DEFAULT NULL, abstract LONGTEXT DEFAULT NULL, keywords VARCHAR(255) DEFAULT NULL, pubstate VARCHAR(50) DEFAULT NULL, bib_tex LONGTEXT DEFAULT NULL, eid VARCHAR(40) DEFAULT NULL, isbn VARCHAR(40) DEFAULT NULL, number VARCHAR(20) DEFAULT NULL, authors VARCHAR(255) DEFAULT NULL, UNIQUE INDEX ix_title_type_year (title, type, year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE home (id INT AUTO_INCREMENT NOT NULL, main_name VARCHAR(255) NOT NULL, extended_name VARCHAR(255) DEFAULT NULL, definition LONGTEXT DEFAULT NULL, logo LONGBLOB DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE researchprojects (id INT AUTO_INCREMENT NOT NULL, participant_responsible_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, reference VARCHAR(100) DEFAULT NULL, financial_entity VARCHAR(255) DEFAULT NULL, amount VARCHAR(255) DEFAULT NULL, participating_entities LONGTEXT DEFAULT NULL, duration LONGTEXT DEFAULT NULL, unesco_codes LONGTEXT DEFAULT NULL, technological_areas LONGTEXT DEFAULT NULL, cnae_codes LONGTEXT DEFAULT NULL, picture1 LONGBLOB DEFAULT NULL, picture2 LONGBLOB DEFAULT NULL, picture3 LONGBLOB DEFAULT NULL, UNIQUE INDEX UNIQ_B175E2C3AEA34913 (reference), INDEX IDX_B175E2C39916F462 (participant_responsible_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE research_project_member (research_project_id INT NOT NULL, member_id INT NOT NULL, INDEX IDX_B17B4E1ED27719F4 (research_project_id), INDEX IDX_B17B4E1E7597D3FE (member_id), PRIMARY KEY(research_project_id, member_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE research_project_collaborator (research_project_id INT NOT NULL, collaborator_id INT NOT NULL, INDEX IDX_2EC9076AD27719F4 (research_project_id), INDEX IDX_2EC9076A30098C8C (collaborator_id), PRIMARY KEY(research_project_id, collaborator_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE collaborators (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, email VARCHAR(235) DEFAULT NULL, phone VARCHAR(40) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE members (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, picture_profile LONGBLOB DEFAULT NULL, qualifications LONGTEXT DEFAULT NULL, occupation LONGTEXT DEFAULT NULL, address LONGTEXT DEFAULT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(40) DEFAULT NULL, fax VARCHAR(40) DEFAULT NULL, subjects LONGTEXT DEFAULT NULL, membership LONGTEXT DEFAULT NULL, google_scholar_id VARCHAR(20) DEFAULT NULL, scopus_id VARCHAR(20) DEFAULT NULL, mendeley_id VARCHAR(50) DEFAULT NULL, orcid_id VARCHAR(19) DEFAULT NULL, researcher_id VARCHAR(20) DEFAULT NULL, h_index INT DEFAULT NULL, personal_web_site VARCHAR(235) DEFAULT NULL, positions LONGTEXT DEFAULT NULL, other LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, roles LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_45A0D2FFE7927C74 (email), UNIQUE INDEX UNIQ_45A0D2FFF85E0677 (username), UNIQUE INDEX ix_first_name_last_name (first_name, last_name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, first_line VARCHAR(255) NOT NULL, second_line VARCHAR(255) DEFAULT NULL, address LONGTEXT DEFAULT NULL, phone VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE researchlines (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, explanation LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_4794CC5D2B36786B (title), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE authorships (id INT AUTO_INCREMENT NOT NULL, member_id INT NOT NULL, publication_id INT NOT NULL, type_of_authorship ENUM(\'author\',\'assignee\',\'editor\',\'chair-or-translator\',\'co-investigator\',\'co-inventor\',\'graduate-student\',\'other-inventor\',\'principal-investigator\',\'postdoctoral-researcher\',\'support-staff\'), INDEX IDX_9E3DE3F57597D3FE (member_id), INDEX IDX_9E3DE3F538B217A7 (publication_id), UNIQUE INDEX ix_member_publication (member_id, publication_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts (id INT AUTO_INCREMENT NOT NULL, author_id INT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, summary VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, published_at DATETIME DEFAULT NULL, INDEX IDX_885DBAFAF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE researchprojects ADD CONSTRAINT FK_B175E2C39916F462 FOREIGN KEY (participant_responsible_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE research_project_member ADD CONSTRAINT FK_B17B4E1ED27719F4 FOREIGN KEY (research_project_id) REFERENCES researchprojects (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE research_project_member ADD CONSTRAINT FK_B17B4E1E7597D3FE FOREIGN KEY (member_id) REFERENCES members (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE research_project_collaborator ADD CONSTRAINT FK_2EC9076AD27719F4 FOREIGN KEY (research_project_id) REFERENCES researchprojects (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE research_project_collaborator ADD CONSTRAINT FK_2EC9076A30098C8C FOREIGN KEY (collaborator_id) REFERENCES collaborators (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE authorships ADD CONSTRAINT FK_9E3DE3F57597D3FE FOREIGN KEY (member_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE authorships ADD CONSTRAINT FK_9E3DE3F538B217A7 FOREIGN KEY (publication_id) REFERENCES publications (id)');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFAF675F31B FOREIGN KEY (author_id) REFERENCES members (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE authorships DROP FOREIGN KEY FK_9E3DE3F538B217A7');
        $this->addSql('ALTER TABLE research_project_member DROP FOREIGN KEY FK_B17B4E1ED27719F4');
        $this->addSql('ALTER TABLE research_project_collaborator DROP FOREIGN KEY FK_2EC9076AD27719F4');
        $this->addSql('ALTER TABLE research_project_collaborator DROP FOREIGN KEY FK_2EC9076A30098C8C');
        $this->addSql('ALTER TABLE researchprojects DROP FOREIGN KEY FK_B175E2C39916F462');
        $this->addSql('ALTER TABLE research_project_member DROP FOREIGN KEY FK_B17B4E1E7597D3FE');
        $this->addSql('ALTER TABLE authorships DROP FOREIGN KEY FK_9E3DE3F57597D3FE');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFAF675F31B');
        $this->addSql('DROP TABLE publications');
        $this->addSql('DROP TABLE home');
        $this->addSql('DROP TABLE researchprojects');
        $this->addSql('DROP TABLE research_project_member');
        $this->addSql('DROP TABLE research_project_collaborator');
        $this->addSql('DROP TABLE collaborators');
        $this->addSql('DROP TABLE members');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE researchlines');
        $this->addSql('DROP TABLE authorships');
        $this->addSql('DROP TABLE posts');
    }
}
