<?php

namespace App\Repository;

use App\Entity\ResearchLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @method ResearchLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResearchLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResearchLine[]    findAll()
 * @method ResearchLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResearchLineRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResearchLine::class);
    }

//    /**
//     * @return ResearchLine[] Returns an array of ResearchLine objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResearchLine
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
