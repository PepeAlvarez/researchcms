<?php

namespace App\Repository;

use App\Entity\Authorship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @method Authorship|null find($id, $lockMode = null, $lockVersion = null)
 * @method Authorship|null findOneBy(array $criteria, array $orderBy = null)
 * @method Authorship[]    findAll()
 * @method Authorship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorshipRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Authorship::class);
    }

//    /**
//     * @return Authorship[] Returns an array of Authorship objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Authorship
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
