<?php

namespace App\Repository;

use App\Entity\ResearchProject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/*
 * This file is part of the ResearchCMS project
 */
/**
 * @author José F. Álvarez Aguilar <josealvarezaguilar@gmail.com>
 */

/**
 * @method ResearchProject|null find($id, $lockMode = null, $lockVersion = null)
 * @method ResearchProject|null findOneBy(array $criteria, array $orderBy = null)
 * @method ResearchProject[]    findAll()
 * @method ResearchProject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResearchProjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ResearchProject::class);
    }

//    /**
//     * @return ResearchProject[] Returns an array of ResearchProject objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ResearchProject
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
