<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'argument_resolver.service' shared service.

include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentValueResolverInterface.php';
include_once $this->targetDirs[3].'/vendor/symfony/http-kernel/Controller/ArgumentResolver/ServiceValueResolver.php';

return $this->privates['argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\ServiceLocator(array('App\\Controller\\ContactController::delete' => function () {
    return ($this->privates['.service_locator.jwthdOp'] ?? $this->load('get_ServiceLocator_JwthdOpService.php'));
}, 'App\\Controller\\ContactController::edit' => function () {
    return ($this->privates['.service_locator.jwthdOp'] ?? $this->load('get_ServiceLocator_JwthdOpService.php'));
}, 'App\\Controller\\ContactController::index' => function () {
    return ($this->privates['.service_locator.ZVwcfpE'] ?? $this->load('get_ServiceLocator_ZVwcfpEService.php'));
}, 'App\\Controller\\ContactController::show' => function () {
    return ($this->privates['.service_locator.jwthdOp'] ?? $this->load('get_ServiceLocator_JwthdOpService.php'));
}, 'App\\Controller\\HomeController::delete' => function () {
    return ($this->privates['.service_locator.wNWlxET'] ?? $this->load('get_ServiceLocator_WNWlxETService.php'));
}, 'App\\Controller\\HomeController::edit' => function () {
    return ($this->privates['.service_locator.wNWlxET'] ?? $this->load('get_ServiceLocator_WNWlxETService.php'));
}, 'App\\Controller\\HomeController::index' => function () {
    return ($this->privates['.service_locator.e25PjnZ'] ?? $this->load('get_ServiceLocator_E25PjnZService.php'));
}, 'App\\Controller\\HomeController::show' => function () {
    return ($this->privates['.service_locator.wNWlxET'] ?? $this->load('get_ServiceLocator_WNWlxETService.php'));
}, 'App\\Controller\\MemberController::delete' => function () {
    return ($this->privates['.service_locator.qPMTUZd'] ?? $this->load('get_ServiceLocator_QPMTUZdService.php'));
}, 'App\\Controller\\MemberController::edit' => function () {
    return ($this->privates['.service_locator.CtGrDuC'] ?? $this->load('get_ServiceLocator_CtGrDuCService.php'));
}, 'App\\Controller\\MemberController::index' => function () {
    return ($this->privates['.service_locator.uO5nSH6'] ?? $this->load('get_ServiceLocator_UO5nSH6Service.php'));
}, 'App\\Controller\\MemberController::new' => function () {
    return ($this->privates['.service_locator.cPEJcDm'] ?? $this->load('get_ServiceLocator_CPEJcDmService.php'));
}, 'App\\Controller\\MemberController::show' => function () {
    return ($this->privates['.service_locator.qPMTUZd'] ?? $this->load('get_ServiceLocator_QPMTUZdService.php'));
}, 'App\\Controller\\NavBarController::index' => function () {
    return ($this->privates['.service_locator.e25PjnZ'] ?? $this->load('get_ServiceLocator_E25PjnZService.php'));
}, 'App\\Controller\\PublicationController::delete' => function () {
    return ($this->privates['.service_locator.1e2LiNE'] ?? $this->load('get_ServiceLocator_1e2LiNEService.php'));
}, 'App\\Controller\\PublicationController::edit' => function () {
    return ($this->privates['.service_locator.1e2LiNE'] ?? $this->load('get_ServiceLocator_1e2LiNEService.php'));
}, 'App\\Controller\\PublicationController::index' => function () {
    return ($this->privates['.service_locator.K7nQcde'] ?? $this->load('get_ServiceLocator_K7nQcdeService.php'));
}, 'App\\Controller\\PublicationController::show' => function () {
    return ($this->privates['.service_locator.1e2LiNE'] ?? $this->load('get_ServiceLocator_1e2LiNEService.php'));
}, 'App\\Controller\\ResearchProjectController::delete' => function () {
    return ($this->privates['.service_locator.cFadsrz'] ?? $this->load('get_ServiceLocator_CFadsrzService.php'));
}, 'App\\Controller\\ResearchProjectController::edit' => function () {
    return ($this->privates['.service_locator.cFadsrz'] ?? $this->load('get_ServiceLocator_CFadsrzService.php'));
}, 'App\\Controller\\ResearchProjectController::index' => function () {
    return ($this->privates['.service_locator.GhHHGvT'] ?? $this->load('get_ServiceLocator_GhHHGvTService.php'));
}, 'App\\Controller\\ResearchProjectController::show' => function () {
    return ($this->privates['.service_locator.cFadsrz'] ?? $this->load('get_ServiceLocator_CFadsrzService.php'));
}, 'App\\Controller\\StartController::index' => function () {
    return ($this->privates['.service_locator.e25PjnZ'] ?? $this->load('get_ServiceLocator_E25PjnZService.php'));
}, 'App\\Controller\\ContactController:delete' => function () {
    return ($this->privates['.service_locator.jwthdOp'] ?? $this->load('get_ServiceLocator_JwthdOpService.php'));
}, 'App\\Controller\\ContactController:edit' => function () {
    return ($this->privates['.service_locator.jwthdOp'] ?? $this->load('get_ServiceLocator_JwthdOpService.php'));
}, 'App\\Controller\\ContactController:index' => function () {
    return ($this->privates['.service_locator.ZVwcfpE'] ?? $this->load('get_ServiceLocator_ZVwcfpEService.php'));
}, 'App\\Controller\\ContactController:show' => function () {
    return ($this->privates['.service_locator.jwthdOp'] ?? $this->load('get_ServiceLocator_JwthdOpService.php'));
}, 'App\\Controller\\HomeController:delete' => function () {
    return ($this->privates['.service_locator.wNWlxET'] ?? $this->load('get_ServiceLocator_WNWlxETService.php'));
}, 'App\\Controller\\HomeController:edit' => function () {
    return ($this->privates['.service_locator.wNWlxET'] ?? $this->load('get_ServiceLocator_WNWlxETService.php'));
}, 'App\\Controller\\HomeController:index' => function () {
    return ($this->privates['.service_locator.e25PjnZ'] ?? $this->load('get_ServiceLocator_E25PjnZService.php'));
}, 'App\\Controller\\HomeController:show' => function () {
    return ($this->privates['.service_locator.wNWlxET'] ?? $this->load('get_ServiceLocator_WNWlxETService.php'));
}, 'App\\Controller\\MemberController:delete' => function () {
    return ($this->privates['.service_locator.qPMTUZd'] ?? $this->load('get_ServiceLocator_QPMTUZdService.php'));
}, 'App\\Controller\\MemberController:edit' => function () {
    return ($this->privates['.service_locator.CtGrDuC'] ?? $this->load('get_ServiceLocator_CtGrDuCService.php'));
}, 'App\\Controller\\MemberController:index' => function () {
    return ($this->privates['.service_locator.uO5nSH6'] ?? $this->load('get_ServiceLocator_UO5nSH6Service.php'));
}, 'App\\Controller\\MemberController:new' => function () {
    return ($this->privates['.service_locator.cPEJcDm'] ?? $this->load('get_ServiceLocator_CPEJcDmService.php'));
}, 'App\\Controller\\MemberController:show' => function () {
    return ($this->privates['.service_locator.qPMTUZd'] ?? $this->load('get_ServiceLocator_QPMTUZdService.php'));
}, 'App\\Controller\\NavBarController:index' => function () {
    return ($this->privates['.service_locator.e25PjnZ'] ?? $this->load('get_ServiceLocator_E25PjnZService.php'));
}, 'App\\Controller\\PublicationController:delete' => function () {
    return ($this->privates['.service_locator.1e2LiNE'] ?? $this->load('get_ServiceLocator_1e2LiNEService.php'));
}, 'App\\Controller\\PublicationController:edit' => function () {
    return ($this->privates['.service_locator.1e2LiNE'] ?? $this->load('get_ServiceLocator_1e2LiNEService.php'));
}, 'App\\Controller\\PublicationController:index' => function () {
    return ($this->privates['.service_locator.K7nQcde'] ?? $this->load('get_ServiceLocator_K7nQcdeService.php'));
}, 'App\\Controller\\PublicationController:show' => function () {
    return ($this->privates['.service_locator.1e2LiNE'] ?? $this->load('get_ServiceLocator_1e2LiNEService.php'));
}, 'App\\Controller\\ResearchProjectController:delete' => function () {
    return ($this->privates['.service_locator.cFadsrz'] ?? $this->load('get_ServiceLocator_CFadsrzService.php'));
}, 'App\\Controller\\ResearchProjectController:edit' => function () {
    return ($this->privates['.service_locator.cFadsrz'] ?? $this->load('get_ServiceLocator_CFadsrzService.php'));
}, 'App\\Controller\\ResearchProjectController:index' => function () {
    return ($this->privates['.service_locator.GhHHGvT'] ?? $this->load('get_ServiceLocator_GhHHGvTService.php'));
}, 'App\\Controller\\ResearchProjectController:show' => function () {
    return ($this->privates['.service_locator.cFadsrz'] ?? $this->load('get_ServiceLocator_CFadsrzService.php'));
}, 'App\\Controller\\StartController:index' => function () {
    return ($this->privates['.service_locator.e25PjnZ'] ?? $this->load('get_ServiceLocator_E25PjnZService.php'));
})));
