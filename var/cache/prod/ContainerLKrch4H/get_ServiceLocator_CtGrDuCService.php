<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.service_locator.CtGrDuC' shared service.

return $this->privates['.service_locator.CtGrDuC'] = new \Symfony\Component\DependencyInjection\ServiceLocator(array('member' => function (): \App\Entity\Member {
    return ($this->privates['.errored..service_locator.CtGrDuC.App\Entity\Member'] ?? $this->load('getMemberService.php'));
}, 'passwordEncoder' => function () {
    return ($this->services['security.password_encoder'] ?? $this->load('getSecurity_PasswordEncoderService.php'));
}));
