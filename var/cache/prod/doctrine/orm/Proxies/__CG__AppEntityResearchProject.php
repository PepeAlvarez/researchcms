<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ResearchProject extends \App\Entity\ResearchProject implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'id', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'title', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'reference', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'financialEntity', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'amount', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'participatingEntities', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'duration', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'participantResponsible', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'participants', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'descriptor', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'unescoCodes', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'technologicalAreas', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'cnaeCodes', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'picture1', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'picture2', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'picture3', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'externalParticipant', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'externalParticipantObject'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'id', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'title', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'reference', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'financialEntity', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'amount', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'participatingEntities', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'duration', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'participantResponsible', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'participants', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'descriptor', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'unescoCodes', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'technologicalAreas', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'cnaeCodes', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'picture1', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'picture2', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'picture3', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'externalParticipant', '' . "\0" . 'App\\Entity\\ResearchProject' . "\0" . 'externalParticipantObject'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ResearchProject $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTitle', []);

        return parent::getTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle(string $title): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTitle', [$title]);

        return parent::setTitle($title);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReference', []);

        return parent::getReference();
    }

    /**
     * {@inheritDoc}
     */
    public function setReference(?string $reference): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReference', [$reference]);

        return parent::setReference($reference);
    }

    /**
     * {@inheritDoc}
     */
    public function getFinancialEntity(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFinancialEntity', []);

        return parent::getFinancialEntity();
    }

    /**
     * {@inheritDoc}
     */
    public function setFinancialEntity(?string $financialEntity): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFinancialEntity', [$financialEntity]);

        return parent::setFinancialEntity($financialEntity);
    }

    /**
     * {@inheritDoc}
     */
    public function getAmount(): ?float
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAmount', []);

        return parent::getAmount();
    }

    /**
     * {@inheritDoc}
     */
    public function setAmount(?float $amount): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAmount', [$amount]);

        return parent::setAmount($amount);
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipatingEntities(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getParticipatingEntities', []);

        return parent::getParticipatingEntities();
    }

    /**
     * {@inheritDoc}
     */
    public function setParticipatingEntities(?string $participatingEntities): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setParticipatingEntities', [$participatingEntities]);

        return parent::setParticipatingEntities($participatingEntities);
    }

    /**
     * {@inheritDoc}
     */
    public function getDuration(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDuration', []);

        return parent::getDuration();
    }

    /**
     * {@inheritDoc}
     */
    public function setDuration(?string $duration): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDuration', [$duration]);

        return parent::setDuration($duration);
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipantResponsible(): ?\App\Entity\Member
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getParticipantResponsible', []);

        return parent::getParticipantResponsible();
    }

    /**
     * {@inheritDoc}
     */
    public function setParticipantResponsible(?\App\Entity\Member $participantResponsible): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setParticipantResponsible', [$participantResponsible]);

        return parent::setParticipantResponsible($participantResponsible);
    }

    /**
     * {@inheritDoc}
     */
    public function getParticipants(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getParticipants', []);

        return parent::getParticipants();
    }

    /**
     * {@inheritDoc}
     */
    public function addParticipant(\App\Entity\Member $participant): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addParticipant', [$participant]);

        return parent::addParticipant($participant);
    }

    /**
     * {@inheritDoc}
     */
    public function removeParticipant(\App\Entity\Member $participant): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeParticipant', [$participant]);

        return parent::removeParticipant($participant);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescriptor(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescriptor', []);

        return parent::getDescriptor();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescriptor(?string $descriptor): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescriptor', [$descriptor]);

        return parent::setDescriptor($descriptor);
    }

    /**
     * {@inheritDoc}
     */
    public function getUnescoCodes(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUnescoCodes', []);

        return parent::getUnescoCodes();
    }

    /**
     * {@inheritDoc}
     */
    public function setUnescoCodes(?string $unescoCodes): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUnescoCodes', [$unescoCodes]);

        return parent::setUnescoCodes($unescoCodes);
    }

    /**
     * {@inheritDoc}
     */
    public function getTechnologicalAreas(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTechnologicalAreas', []);

        return parent::getTechnologicalAreas();
    }

    /**
     * {@inheritDoc}
     */
    public function setTechnologicalAreas(?string $technologicalAreas): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTechnologicalAreas', [$technologicalAreas]);

        return parent::setTechnologicalAreas($technologicalAreas);
    }

    /**
     * {@inheritDoc}
     */
    public function getCnaeCodes(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCnaeCodes', []);

        return parent::getCnaeCodes();
    }

    /**
     * {@inheritDoc}
     */
    public function setCnaeCodes(?string $cnaeCodes): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCnaeCodes', [$cnaeCodes]);

        return parent::setCnaeCodes($cnaeCodes);
    }

    /**
     * {@inheritDoc}
     */
    public function getPicture1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPicture1', []);

        return parent::getPicture1();
    }

    /**
     * {@inheritDoc}
     */
    public function setPicture1($picture1): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPicture1', [$picture1]);

        return parent::setPicture1($picture1);
    }

    /**
     * {@inheritDoc}
     */
    public function getPicture2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPicture2', []);

        return parent::getPicture2();
    }

    /**
     * {@inheritDoc}
     */
    public function setPicture2($picture2): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPicture2', [$picture2]);

        return parent::setPicture2($picture2);
    }

    /**
     * {@inheritDoc}
     */
    public function getPicture3()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPicture3', []);

        return parent::getPicture3();
    }

    /**
     * {@inheritDoc}
     */
    public function setPicture3($picture3): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPicture3', [$picture3]);

        return parent::setPicture3($picture3);
    }

    /**
     * {@inheritDoc}
     */
    public function getExternalParticipant(): ?array
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExternalParticipant', []);

        return parent::getExternalParticipant();
    }

    /**
     * {@inheritDoc}
     */
    public function setExternalParticipant(?array $externalParticipant): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExternalParticipant', [$externalParticipant]);

        return parent::setExternalParticipant($externalParticipant);
    }

    /**
     * {@inheritDoc}
     */
    public function getExternalParticipantObject()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExternalParticipantObject', []);

        return parent::getExternalParticipantObject();
    }

    /**
     * {@inheritDoc}
     */
    public function setExternalParticipantObject($externalParticipantObject): \App\Entity\ResearchProject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExternalParticipantObject', [$externalParticipantObject]);

        return parent::setExternalParticipantObject($externalParticipantObject);
    }

}
