<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'authorship_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::index'), array(), array(array('text', '/authorship/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'authorship_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::new'), array(), array(array('text', '/authorship/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'authorship_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/authorship'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'authorship_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/authorship'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'authorship_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/authorship'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_pub' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::pub'), array(), array(array('text', '/collaborator/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_pub_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::pubshow'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/collaborator/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::index'), array(), array(array('text', '/collaborator/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::new'), array(), array(array('text', '/collaborator/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/collaborator'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/collaborator'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'collaborator_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/collaborator'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'contact_pub' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::pub'), array(), array(array('text', '/contact/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'contact_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::index'), array(), array(array('text', '/contact/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'contact_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::new'), array(), array(array('text', '/contact/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'contact_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/contact'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'contact_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/contact'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'contact_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/contact'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'home_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::index'), array(), array(array('text', '/home/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'home_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::new'), array(), array(array('text', '/home/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'home_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/home'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'home_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/home'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'home_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/home'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_pub' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::pub'), array(), array(array('text', '/member/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_pub_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::pubshow'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/member/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::index'), array(), array(array('text', '/member/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::new'), array(), array(array('text', '/member/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/member'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_edit_self' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::edit_self'), array(), array(array('text', '/editself'), array('variable', '/', '[^/]++', 'id'), array('text', '/member'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/member'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'member_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/member'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_pub' => array(array('_locale', 'currentPage', 'limit'), array('_locale' => 'en', 'currentPage' => 1, 'limit' => 4, '_controller' => 'App\\Controller\\PostController::pub'), array('currentPage' => '\\d+'), array(array('variable', '/', '[^/]++', 'limit'), array('variable', '/', '\\d+', 'currentPage'), array('text', '/post/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_pub_show' => array(array('_locale', 'slug'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PostController::pubshow'), array(), array(array('variable', '/', '[^/]++', 'slug'), array('text', '/post/pub/show'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PostController::index'), array(), array(array('text', '/post/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PostController::new'), array(), array(array('text', '/post/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PostController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/post'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PostController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/post'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'post_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PostController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/post'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_pub' => array(array('_locale', 'currentPage', 'limit'), array('_locale' => 'en', 'currentPage' => 1, 'limit' => 4, '_controller' => 'App\\Controller\\PublicationController::pub'), array('currentPage' => '\\d+'), array(array('variable', '/', '[^/]++', 'limit'), array('variable', '/', '\\d+', 'currentPage'), array('text', '/publication/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_pub_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::pubshow'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/publication/pub/show'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::index'), array(), array(array('text', '/publication/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::new'), array(), array(array('text', '/publication/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/publication'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/publication'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'publication_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/publication'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_line_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::index'), array(), array(array('text', '/research/line/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_line_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::new'), array(), array(array('text', '/research/line/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_line_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/research/line'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_line_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/research/line'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_line_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/research/line'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_pub' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::pub'), array(), array(array('text', '/research/project/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_pub_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::pubshow'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/research/project/pub'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::index'), array(), array(array('text', '/research/project/'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_new' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::new'), array(), array(array('text', '/research/project/new'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_show' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::show'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/research/project'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_edit' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::edit'), array(), array(array('text', '/edit'), array('variable', '/', '[^/]++', 'id'), array('text', '/research/project'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'research_project_delete' => array(array('_locale', 'id'), array('_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::delete'), array(), array(array('variable', '/', '[^/]++', 'id'), array('text', '/research/project'), array('variable', '/', '[^/]++', '_locale')), array(), array()),
        'login' => array(array(), array('_controller' => 'App\\Controller\\SecurityController::login'), array(), array(array('text', '/login')), array(), array()),
        'app_start_index' => array(array('_locale'), array('_locale' => 'en', '_controller' => 'App\\Controller\\StartController::index'), array(), array(array('variable', '/', '[^/]++', '_locale')), array(), array()),
        '_twig_error_test' => array(array('code', '_format'), array('_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code' => '\\d+'), array(array('variable', '.', '[^/]++', '_format'), array('variable', '/', '\\d+', 'code'), array('text', '/_error')), array(), array()),
        '_wdt' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::toolbarAction'), array(), array(array('variable', '/', '[^/]++', 'token'), array('text', '/_wdt')), array(), array()),
        '_profiler_home' => array(array(), array('_controller' => 'web_profiler.controller.profiler::homeAction'), array(), array(array('text', '/_profiler/')), array(), array()),
        '_profiler_search' => array(array(), array('_controller' => 'web_profiler.controller.profiler::searchAction'), array(), array(array('text', '/_profiler/search')), array(), array()),
        '_profiler_search_bar' => array(array(), array('_controller' => 'web_profiler.controller.profiler::searchBarAction'), array(), array(array('text', '/_profiler/search_bar')), array(), array()),
        '_profiler_phpinfo' => array(array(), array('_controller' => 'web_profiler.controller.profiler::phpinfoAction'), array(), array(array('text', '/_profiler/phpinfo')), array(), array()),
        '_profiler_search_results' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array(), array(array('text', '/search/results'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_open_file' => array(array(), array('_controller' => 'web_profiler.controller.profiler::openAction'), array(), array(array('text', '/_profiler/open')), array(), array()),
        '_profiler' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::panelAction'), array(), array(array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_router' => array(array('token'), array('_controller' => 'web_profiler.controller.router::panelAction'), array(), array(array('text', '/router'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_exception' => array(array('token'), array('_controller' => 'web_profiler.controller.exception::showAction'), array(), array(array('text', '/exception'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_exception_css' => array(array('token'), array('_controller' => 'web_profiler.controller.exception::cssAction'), array(), array(array('text', '/exception.css'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        'logout' => array(array(), array(), array(), array(array('text', '/logout')), array(), array()),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && (self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
            unset($parameters['_locale']);
            $name .= '.'.$locale;
        } elseif (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
