<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = $allowSchemes = array();
        if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
            return $ret;
        }
        if ($allow) {
            throw new MethodNotAllowedException(array_keys($allow));
        }
        if (!in_array($this->context->getMethod(), array('HEAD', 'GET'), true)) {
            // no-op
        } elseif ($allowSchemes) {
            redirect_scheme:
            $scheme = $this->context->getScheme();
            $this->context->setScheme(key($allowSchemes));
            try {
                if ($ret = $this->doMatch($pathinfo)) {
                    return $this->redirect($pathinfo, $ret['_route'], $this->context->getScheme()) + $ret;
                }
            } finally {
                $this->context->setScheme($scheme);
            }
        } elseif ('/' !== $pathinfo) {
            $pathinfo = '/' !== $pathinfo[-1] ? $pathinfo.'/' : substr($pathinfo, 0, -1);
            if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
                return $this->redirect($pathinfo, $ret['_route']) + $ret;
            }
            if ($allowSchemes) {
                goto redirect_scheme;
            }
        }

        throw new ResourceNotFoundException();
    }

    private function doMatch(string $rawPathinfo, array &$allow = array(), array &$allowSchemes = array()): ?array
    {
        $allow = $allowSchemes = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        switch ($pathinfo) {
            default:
                $routes = array(
                    '/login' => array(array('_route' => 'login', '_controller' => 'App\\Controller\\SecurityController::login'), null, null, null),
                    '/_profiler/' => array(array('_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'), null, null, null),
                    '/_profiler/search' => array(array('_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'), null, null, null),
                    '/_profiler/search_bar' => array(array('_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'), null, null, null),
                    '/_profiler/phpinfo' => array(array('_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'), null, null, null),
                    '/_profiler/open' => array(array('_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'), null, null, null),
                );

                if (!isset($routes[$pathinfo])) {
                    break;
                }
                list($ret, $requiredHost, $requiredMethods, $requiredSchemes) = $routes[$pathinfo];

                $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                    if ($hasRequiredScheme) {
                        $allow += $requiredMethods;
                    }
                    break;
                }
                if (!$hasRequiredScheme) {
                    $allowSchemes += $requiredSchemes;
                    break;
                }

                return $ret;
        }

        $matchedPathinfo = $pathinfo;
        $regexList = array(
            0 => '{^(?'
                    .'|/([^/]++)/(?'
                        .'|authorship/(?'
                            .'|(*:34)'
                            .'|new(*:44)'
                            .'|([^/]++)(?'
                                .'|(*:62)'
                                .'|/edit(*:74)'
                                .'|(*:81)'
                            .')'
                        .')'
                        .'|co(?'
                            .'|llaborator/(?'
                                .'|pub(?'
                                    .'|(*:115)'
                                    .'|/([^/]++)(*:132)'
                                .')'
                                .'|(*:141)'
                                .'|new(*:152)'
                                .'|([^/]++)(?'
                                    .'|(*:171)'
                                    .'|/edit(*:184)'
                                    .'|(*:192)'
                                .')'
                            .')'
                            .'|ntact/(?'
                                .'|pub(*:214)'
                                .'|(*:222)'
                                .'|new(*:233)'
                                .'|([^/]++)(?'
                                    .'|(*:252)'
                                    .'|/edit(*:265)'
                                    .'|(*:273)'
                                .')'
                            .')'
                        .')'
                        .'|home/(?'
                            .'|(*:292)'
                            .'|new(*:303)'
                            .'|([^/]++)(?'
                                .'|(*:322)'
                                .'|/edit(*:335)'
                                .'|(*:343)'
                            .')'
                        .')'
                        .'|member/(?'
                            .'|pub(?'
                                .'|(*:369)'
                                .'|/([^/]++)(*:386)'
                            .')'
                            .'|(*:395)'
                            .'|new(*:406)'
                            .'|([^/]++)(?'
                                .'|(*:425)'
                                .'|/edit(?'
                                    .'|self(*:445)'
                                    .'|(*:453)'
                                .')'
                                .'|(*:462)'
                            .')'
                        .')'
                        .'|p(?'
                            .'|ost/(?'
                                .'|pub(?'
                                    .'|(?:/(\\d+)(?:/([^/]++))?)?(*:514)'
                                    .'|/show/([^/]++)(*:536)'
                                .')'
                                .'|(*:545)'
                                .'|new(*:556)'
                                .'|([^/]++)(?'
                                    .'|(*:575)'
                                    .'|/edit(*:588)'
                                    .'|(*:596)'
                                .')'
                            .')'
                            .'|ublication/(?'
                                .'|pub(?'
                                    .'|(?:/(\\d+)(?:/([^/]++))?)?(*:651)'
                                    .'|/show/([^/]++)(*:673)'
                                .')'
                                .'|(*:682)'
                                .'|new(*:693)'
                                .'|([^/]++)(?'
                                    .'|(*:712)'
                                    .'|/edit(*:725)'
                                    .'|(*:733)'
                                .')'
                            .')'
                        .')'
                        .'|research/(?'
                            .'|line/(?'
                                .'|(*:764)'
                                .'|new(*:775)'
                                .'|([^/]++)(?'
                                    .'|(*:794)'
                                    .'|/edit(*:807)'
                                    .'|(*:815)'
                                .')'
                            .')'
                            .'|project/(?'
                                .'|pub(?'
                                    .'|(*:842)'
                                    .'|/([^/]++)(*:859)'
                                .')'
                                .'|(*:868)'
                                .'|new(*:879)'
                                .'|([^/]++)(?'
                                    .'|(*:898)'
                                    .'|/edit(*:911)'
                                    .'|(*:919)'
                                .')'
                            .')'
                        .')'
                    .')'
                    .'|/([^/]++)?(*:941)'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:980)'
                        .'|wdt/([^/]++)(*:1000)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:1047)'
                                .'|router(*:1062)'
                                .'|exception(?'
                                    .'|(*:1083)'
                                    .'|\\.css(*:1097)'
                                .')'
                            .')'
                            .'|(*:1108)'
                        .')'
                    .')'
                    .'|/logout(*:1126)'
                .')$}sD',
        );

        foreach ($regexList as $offset => $regex) {
            while (preg_match($regex, $matchedPathinfo, $matches)) {
                switch ($m = (int) $matches['MARK']) {
                    default:
                        $routes = array(
                            34 => array(array('_route' => 'authorship_index', '_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::index'), array('_locale'), array('GET' => 0), null),
                            44 => array(array('_route' => 'authorship_new', '_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            62 => array(array('_route' => 'authorship_show', '_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            74 => array(array('_route' => 'authorship_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            81 => array(array('_route' => 'authorship_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\AuthorshipController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            115 => array(array('_route' => 'collaborator_pub', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::pub'), array('_locale'), array('GET' => 0), null),
                            132 => array(array('_route' => 'collaborator_pub_show', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::pubshow'), array('_locale', 'id'), array('GET' => 0), null),
                            141 => array(array('_route' => 'collaborator_index', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::index'), array('_locale'), array('GET' => 0), null),
                            152 => array(array('_route' => 'collaborator_new', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            171 => array(array('_route' => 'collaborator_show', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            184 => array(array('_route' => 'collaborator_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            192 => array(array('_route' => 'collaborator_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\CollaboratorController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            214 => array(array('_route' => 'contact_pub', '_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::pub'), array('_locale'), array('GET' => 0), null),
                            222 => array(array('_route' => 'contact_index', '_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::index'), array('_locale'), array('GET' => 0), null),
                            233 => array(array('_route' => 'contact_new', '_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            252 => array(array('_route' => 'contact_show', '_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            265 => array(array('_route' => 'contact_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            273 => array(array('_route' => 'contact_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\ContactController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            292 => array(array('_route' => 'home_index', '_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::index'), array('_locale'), array('GET' => 0), null),
                            303 => array(array('_route' => 'home_new', '_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            322 => array(array('_route' => 'home_show', '_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            335 => array(array('_route' => 'home_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            343 => array(array('_route' => 'home_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\HomeController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            369 => array(array('_route' => 'member_pub', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::pub'), array('_locale'), array('GET' => 0), null),
                            386 => array(array('_route' => 'member_pub_show', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::pubshow'), array('_locale', 'id'), array('GET' => 0), null),
                            395 => array(array('_route' => 'member_index', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::index'), array('_locale'), array('GET' => 0), null),
                            406 => array(array('_route' => 'member_new', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            425 => array(array('_route' => 'member_show', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            445 => array(array('_route' => 'member_edit_self', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::edit_self'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            453 => array(array('_route' => 'member_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            462 => array(array('_route' => 'member_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\MemberController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            514 => array(array('_route' => 'post_pub', '_locale' => 'en', 'currentPage' => 1, 'limit' => 4, '_controller' => 'App\\Controller\\PostController::pub'), array('_locale', 'currentPage', 'limit'), array('GET' => 0), null),
                            536 => array(array('_route' => 'post_pub_show', '_locale' => 'en', '_controller' => 'App\\Controller\\PostController::pubshow'), array('_locale', 'slug'), array('GET' => 0), null),
                            545 => array(array('_route' => 'post_index', '_locale' => 'en', '_controller' => 'App\\Controller\\PostController::index'), array('_locale'), array('GET' => 0), null),
                            556 => array(array('_route' => 'post_new', '_locale' => 'en', '_controller' => 'App\\Controller\\PostController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            575 => array(array('_route' => 'post_show', '_locale' => 'en', '_controller' => 'App\\Controller\\PostController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            588 => array(array('_route' => 'post_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\PostController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            596 => array(array('_route' => 'post_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\PostController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            651 => array(array('_route' => 'publication_pub', '_locale' => 'en', 'currentPage' => 1, 'limit' => 4, '_controller' => 'App\\Controller\\PublicationController::pub'), array('_locale', 'currentPage', 'limit'), array('GET' => 0), null),
                            673 => array(array('_route' => 'publication_pub_show', '_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::pubshow'), array('_locale', 'id'), array('GET' => 0), null),
                            682 => array(array('_route' => 'publication_index', '_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::index'), array('_locale'), array('GET' => 0), null),
                            693 => array(array('_route' => 'publication_new', '_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            712 => array(array('_route' => 'publication_show', '_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            725 => array(array('_route' => 'publication_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            733 => array(array('_route' => 'publication_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\PublicationController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            764 => array(array('_route' => 'research_line_index', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::index'), array('_locale'), array('GET' => 0), null),
                            775 => array(array('_route' => 'research_line_new', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            794 => array(array('_route' => 'research_line_show', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            807 => array(array('_route' => 'research_line_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            815 => array(array('_route' => 'research_line_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchLineController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            842 => array(array('_route' => 'research_project_pub', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::pub'), array('_locale'), array('GET' => 0), null),
                            859 => array(array('_route' => 'research_project_pub_show', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::pubshow'), array('_locale', 'id'), array('GET' => 0), null),
                            868 => array(array('_route' => 'research_project_index', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::index'), array('_locale'), array('GET' => 0), null),
                            879 => array(array('_route' => 'research_project_new', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::new'), array('_locale'), array('GET' => 0, 'POST' => 1), null),
                            898 => array(array('_route' => 'research_project_show', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::show'), array('_locale', 'id'), array('GET' => 0), null),
                            911 => array(array('_route' => 'research_project_edit', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::edit'), array('_locale', 'id'), array('GET' => 0, 'POST' => 1), null),
                            919 => array(array('_route' => 'research_project_delete', '_locale' => 'en', '_controller' => 'App\\Controller\\ResearchProjectController::delete'), array('_locale', 'id'), array('DELETE' => 0), null),
                            941 => array(array('_route' => 'app_start_index', '_locale' => 'en', '_controller' => 'App\\Controller\\StartController::index'), array('_locale'), array('GET' => 0), null),
                            980 => array(array('_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code', '_format'), null, null),
                            1000 => array(array('_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'), array('token'), null, null),
                            1047 => array(array('_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array('token'), null, null),
                            1062 => array(array('_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'), array('token'), null, null),
                            1083 => array(array('_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'), array('token'), null, null),
                            1097 => array(array('_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'), array('token'), null, null),
                            1108 => array(array('_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'), array('token'), null, null),
                            1126 => array(array('_route' => 'logout'), array(), null, null),
                        );

                        list($ret, $vars, $requiredMethods, $requiredSchemes) = $routes[$m];

                        foreach ($vars as $i => $v) {
                            if (isset($matches[1 + $i])) {
                                $ret[$v] = $matches[1 + $i];
                            }
                        }

                        $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                        if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                            if ($hasRequiredScheme) {
                                $allow += $requiredMethods;
                            }
                            break;
                        }
                        if (!$hasRequiredScheme) {
                            $allowSchemes += $requiredSchemes;
                            break;
                        }

                        return $ret;
                }

                if (1126 === $m) {
                    break;
                }
                $regex = substr_replace($regex, 'F', $m - $offset, 1 + strlen($m));
                $offset += strlen($m);
            }
        }
        if ('/' === $pathinfo && !$allow && !$allowSchemes) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        return null;
    }
}
