var $collectionHolder;

// setup an "add an authorship" link
var $addAuthorshipButton = $('<br><button type="button" class="add_authorship_link btn btn-success">Add an authorship</button>');
var $newLinkLi = $('<li></li>').append($addAuthorshipButton);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of authorships
    $collectionHolder = $('ul.authorships');

    // add a delete link to all of the existing authorship form li elements
    $collectionHolder.find('li').each(function() {
        addAuthorshipFormDeleteLink($(this));
    });

    // add the "add an authorship" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addAuthorshipButton.on('click', function(e) {
        // add a new authorship form (see next code block)
        addAuthorshipForm($collectionHolder, $newLinkLi);
    });

});

function addAuthorshipForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add an authorship" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);

    // add a delete link to the new form
    addAuthorshipFormDeleteLink($newFormLi);
}

function addAuthorshipFormDeleteLink($tagFormLi) {
    var $removeFormButton = $('<button type="button" class="btn btn-danger">Delete this authorship</button>');
    $tagFormLi.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the li for the authorship form
        $tagFormLi.remove();
    });
}

function removeAuthorshipsNode() {
    // Removes an element from the document
    var element = document.getElementById("member_authorships");
    var parent = element.parentNode;
    parent.parentNode.removeChild(parent);
    //element.parentNode.removeChild(element);
}